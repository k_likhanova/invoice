package by.training.invoice.command.postimpl;

import by.training.invoice.command.Command;
import by.training.invoice.util.LocaleResourceBundle;
import by.training.invoice.command.Router;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Locale command.
 * Created on 14.01.2019
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class LocaleCommand implements Command {
    /**
     * Field specifies locale parameter.
     */
    private static final String LOCALE = "locale";
    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        Router router = new Router();
        LocaleResourceBundle rb =
                LocaleResourceBundle.valueOf(request.getParameter(LOCALE).toUpperCase());

        request.getSession().setAttribute(LOCALE, rb.getLocale());
        request.getSession().setAttribute("rb", rb);
        router.setRoute(Router.RouteType.REDIRECT);
        String path = request.getRequestURI().substring(request.getContextPath().length());
        router.setPagePath(path);
        return router;
    }
}
