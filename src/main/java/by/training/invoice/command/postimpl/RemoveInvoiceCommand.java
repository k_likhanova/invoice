package by.training.invoice.command.postimpl;

import by.training.invoice.command.Command;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.InvoiceLogic;
import by.training.invoice.util.LocaleResourceBundle;
import by.training.invoice.util.Message;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.util.RequestParameter;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The type Remove invoice command.
 * Created on 18.01.2019.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class RemoveInvoiceCommand implements Command {
    /**
     * Field specifies object of Invoice logic.
     */
    private final InvoiceLogic logic = new InvoiceLogic();

    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(RequestParameter.USER_ATTRIBUTE);
        LocaleResourceBundle rb = (LocaleResourceBundle) session
                .getAttribute(Message.RESOURCE_BUNDLE_ATTRIBUTE);
        long invoiceId = Long.parseLong(request.getParameter(RequestParameter.INVOICE_ID_PARAMETER));
        try {
            if (!logic.remove(invoiceId, user.getId())) {
                session.setAttribute(Message.ERROR_MESSAGE,
                        rb.getMessage("message.invoice_is_not_paid"));
            }
        } catch (LogicException e) {
            log.error(e);
            request.setAttribute("errorMessage", e.getMessage());
            router.setPagePath(PagePath.ERROR);
            return router;
        }
        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath(PagePath.INVOICES_HTML);
        return router;
    }
}