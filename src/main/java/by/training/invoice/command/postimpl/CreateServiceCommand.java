package by.training.invoice.command.postimpl;

import by.training.invoice.command.Command;
import by.training.invoice.util.LocaleResourceBundle;
import by.training.invoice.util.Message;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.ServiceLogic;
import by.training.invoice.util.RequestParameter;
import by.training.invoice.validator.InputEmptyValidator;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Create service command.
 * Created on 07.01.2019.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class CreateServiceCommand implements Command {
    /**
     * Field specifies object of Service logic.
     */
    private final ServiceLogic logic = new ServiceLogic();

    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(RequestParameter.USER_ATTRIBUTE);
        Router router = new Router();
        String title = request.getParameter(RequestParameter.TITLE_PARAMETER)
                .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
        String description = request.getParameter(RequestParameter.DESCRIPTION_PARAMETER)
                .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
        String price = request.getParameter(RequestParameter.PRICE_PARAMETER)
                .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
        long userProfileId = (Long) request.getAttribute(RequestParameter.USER_ID_ATTRIBUTE);
        LocaleResourceBundle rb = (LocaleResourceBundle) session
                .getAttribute(Message.RESOURCE_BUNDLE_ATTRIBUTE);

        if (!InputEmptyValidator.isEmptyServiceData(title, price)
                && user.getId() == userProfileId) {
            try {
                if(!logic.create(title, description, price, userProfileId)) {
                    session.setAttribute(Message.ERROR_MESSAGE,
                            rb.getMessage("message.incorrect_money_format"));
                }
            } catch (LogicException e) {
                log.error(e);
                request.setAttribute("errorMessage", e.getMessage());
                router.setPagePath(PagePath.ERROR);
                return router;
            }
        } else if (user.getId() == userProfileId){
            session.setAttribute(Message.ERROR_MESSAGE,
                    rb.getMessage("message.not_all_info"));
        } else {
            session.setAttribute(Message.ERROR_MESSAGE,
                    rb.getMessage("message.no_access"));
        }
        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath(String.format(PagePath.PROFILE_HTML, userProfileId));
        return router;
    }
}
