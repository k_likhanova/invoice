package by.training.invoice.command.postimpl;

import by.training.invoice.command.Command;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;

import javax.servlet.http.HttpServletRequest;

/**
 * Logout command.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 */
public class LogoutCommand implements Command {
    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        Router router = new Router();
        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath(PagePath.LOGIN_HTML);
        request.getSession().setAttribute("user", null);
        return router;
    }
}
