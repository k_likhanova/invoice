package by.training.invoice.command.postimpl;

import by.training.invoice.command.Command;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.UserLogic;
import by.training.invoice.util.LocaleResourceBundle;
import by.training.invoice.util.Message;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.util.RequestParameter;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

/**
 * Recharge balance command.
 * Created on 12.01.2019.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class RechargeBalanceCommand implements Command {
    /**
     * Field specifies object of User logic.
     */
    private final UserLogic logic = new UserLogic();

    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        HttpSession session = request.getSession();
        User sessionUser = (User) session.getAttribute(RequestParameter.USER_ATTRIBUTE);
        Router router = new Router();

        String moneyAmountInString = request.getParameter(RequestParameter.MONEY_AMOUNT_PARAMETER)
                .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
        long userProfileId = (Long) request.getAttribute(RequestParameter.USER_ID_ATTRIBUTE);
        LocaleResourceBundle rb = (LocaleResourceBundle) session
                                .getAttribute(Message.RESOURCE_BUNDLE_ATTRIBUTE);
        BigDecimal moneyAmount;

        if (!moneyAmountInString.isEmpty() && sessionUser.getId() == userProfileId) {
            try {
                moneyAmount = logic.rechargeUserBalance(moneyAmountInString,
                                                        userProfileId);
            } catch (LogicException e) {
                log.error(e);
                request.setAttribute("errorMessage", e.getMessage());
                router.setPagePath(PagePath.ERROR);
                return router;
            }
            if (moneyAmount != null) {
                sessionUser.setCurrentBalance(moneyAmount);
            } else {
                session.setAttribute(Message.ERROR_MESSAGE,
                        rb.getMessage("message.incorrect_money_format"));
            }
        } else if (sessionUser.getId() == userProfileId){
            session.setAttribute(Message.ERROR_MESSAGE,
                                rb.getMessage("message.not_all_info"));
        } else {
            session.setAttribute(Message.ERROR_MESSAGE,
                                rb.getMessage("message.no_access"));
        }

        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath(String.format(PagePath.PROFILE_HTML, userProfileId));
        return router;
    }
}
