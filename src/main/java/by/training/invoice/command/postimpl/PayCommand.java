package by.training.invoice.command.postimpl;

import by.training.invoice.command.Command;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.InvoiceLogic;
import by.training.invoice.util.LocaleResourceBundle;
import by.training.invoice.util.Message;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.util.RequestParameter;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

/**
 * The type Pay command.
 * Created on 13.01.2019.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class PayCommand implements Command {
    /**
     * Field specifies object of Invoice logic.
     */
    private final InvoiceLogic logic = new InvoiceLogic();

    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(RequestParameter.USER_ATTRIBUTE);
        Router router = new Router();
        String payment = request.getParameter(RequestParameter.PAYMENT_PARAMETER)
                .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
        long invoiceId = (Long) request.getAttribute(RequestParameter.INVOICE_ID_ATTRIBUTE);
        long customerId = Long.parseLong(request.getParameter(RequestParameter.CUSTOMER_ID_PARAMETER));
        LocaleResourceBundle rb = (LocaleResourceBundle) session
                                    .getAttribute(Message.RESOURCE_BUNDLE_ATTRIBUTE);

        if (!payment.isEmpty()
                && customerId == user.getId()) {
            try {
                BigDecimal result = logic.pay(payment, invoiceId);
                if(result == null) {
                    session.setAttribute(Message.ERROR_MESSAGE,
                            rb.getMessage("message.incorrect_money_format"));
                } else if(result.compareTo(BigDecimal.ONE.negate()) == 0) {
                    session.setAttribute(Message.ERROR_MESSAGE,
                            rb.getMessage("message.not_enough_money"));
                } else if (result.compareTo(BigDecimal.ZERO) == 0) {
                    session.setAttribute(Message.ERROR_MESSAGE,
                            rb.getMessage("message.paid_invoice"));
                }
            } catch (LogicException e) {
                log.error(e);
                request.setAttribute("errorMessage", e.getMessage());
                router.setPagePath(PagePath.ERROR);
                return router;
            }
        } else if (customerId == user.getId()){
            session.setAttribute(Message.ERROR_MESSAGE,
                    rb.getMessage("message.not_all_info"));
        } else {
            session.setAttribute(Message.ERROR_MESSAGE,
                    rb.getMessage("message.no_access"));
        }

        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath(String.format(PagePath.INVOICE_HTML, invoiceId));
        return router;
    }
}
