package by.training.invoice.command.postimpl;

import by.training.invoice.command.Command;
import by.training.invoice.entity.impl.User;
import by.training.invoice.util.LocaleResourceBundle;
import by.training.invoice.util.Message;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.UserLogic;
import by.training.invoice.util.RequestParameter;
import by.training.invoice.validator.InputEmptyValidator;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Registration command.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class RegistrationCommand implements Command {
    /**
     * Field specifies object of User logic.
     */
    private final UserLogic logic = new UserLogic();

    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        Router router = new Router();
        router.setRoute(Router.RouteType.REDIRECT);
        HttpSession session = request.getSession();
        User sessionUser = (User) session.getAttribute("user");
        LocaleResourceBundle rb = (LocaleResourceBundle) session
                                    .getAttribute(Message.RESOURCE_BUNDLE_ATTRIBUTE);
        if (sessionUser == null) {
            String email = request.getParameter(RequestParameter.EMAIL_PARAMETER)
                    .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
            String password = request.getParameter(RequestParameter.PASSWORD_PARAMETER)
                    .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
            String name = request.getParameter(RequestParameter.NAME_PARAMETER)
                    .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
            String phone = request.getParameter(RequestParameter.PHONE_PARAMETER)
                    .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);

            if (rb == null) {
                rb = LocaleResourceBundle.EN;
                session.setAttribute("rb", rb);
            }

            if (!InputEmptyValidator.isEmptyLoginData(email, password)
                    && !InputEmptyValidator.isEmptyUserData(name, phone)) {
                try {
                    switch (logic.registration(email, password, name, phone)) {
                        case 1:
                            session.setAttribute(Message.SUCCESSFUL_REGISTRATION_MESSAGE,
                                    rb.getMessage("message.successful_registration"));
                            router.setPagePath(PagePath.LOGIN_HTML);
                            break;
                        case -1:
                            session.setAttribute(Message.ERROR_REGISTRATION_MESSAGE,
                                    rb.getMessage("message.incorrect_user_data"));
                            router.setPagePath(PagePath.REGISTRATION_HTML);
                            break;
                        case 0:
                            session.setAttribute(Message.ERROR_REGISTRATION_MESSAGE,
                                    rb.getMessage("message.exist_email") + email);
                            router.setPagePath(PagePath.REGISTRATION_HTML);
                            break;
                        default: break;
                    }
                } catch (LogicException e) {
                    log.error(e);
                    request.setAttribute("errorMessage", e.getMessage());
                    router.setRoute(Router.RouteType.FORWARD);
                    router.setPagePath(PagePath.ERROR);
                    return router;
                }
            } else {
                session.setAttribute(Message.ERROR_REGISTRATION_MESSAGE,
                                    rb.getMessage("message.not_all_info"));
                router.setPagePath(PagePath.REGISTRATION_HTML);
            }
        } else {
            router.setPagePath(String.format(PagePath.PROFILE_HTML, sessionUser.getId()));
        }
        return router;
    }
}
