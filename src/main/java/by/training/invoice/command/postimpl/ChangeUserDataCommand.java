package by.training.invoice.command.postimpl;

import by.training.invoice.command.Command;
import by.training.invoice.util.LocaleResourceBundle;
import by.training.invoice.util.Message;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.UserLogic;
import by.training.invoice.util.RequestParameter;
import by.training.invoice.validator.InputEmptyValidator;
import by.training.invoice.validator.UserValidator;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Change user data command.
 * Created on 29.12.2018
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class ChangeUserDataCommand implements Command {
    /**
     * Field specifies object of User logic.
     */
    private final UserLogic logic = new UserLogic();

    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        User sessionUser = (User) session.getAttribute(RequestParameter.USER_ATTRIBUTE);
        LocaleResourceBundle rb = (LocaleResourceBundle) session
                .getAttribute(Message.RESOURCE_BUNDLE_ATTRIBUTE);

        long requestUserId = (Long) request.getAttribute(RequestParameter.USER_ID_ATTRIBUTE);
        String requestEmail = request.getParameter(RequestParameter.EMAIL_PARAMETER)
                                .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
        String requestName = request.getParameter(RequestParameter.NAME_PARAMETER)
                                .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
        String requestPhone = request.getParameter(RequestParameter.PHONE_PARAMETER)
                                .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);

        String email = sessionUser.getEmail();
        String name = sessionUser.getName();
        String phone = sessionUser.getPhone();

        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath(String.format(PagePath.PROFILE_HTML, requestUserId));

        if (InputEmptyValidator.isEmptyAllUserData(requestEmail, requestName, requestPhone)
                || !UserValidator.isChangedData(email, name, phone,
                                        requestEmail, requestName, requestPhone)) {
            return router;
        }
        if (sessionUser.getId() == requestUserId) {

            if (!requestName.isEmpty()) {
                name = requestName;
            }
            if (!requestPhone.isEmpty()) {
                phone = requestPhone;
            }
            boolean emailChanged = false;
            if (!requestEmail.isEmpty()
                    && !requestEmail.equals(sessionUser.getEmail())) {
                emailChanged = true;
                email = requestEmail;
            }

            String updatingMessage;
            try {
                updatingMessage = logic.updateUserData(email, name, phone,
                                            sessionUser.getId(), emailChanged);
            } catch (LogicException e) {
                log.error(e);
                router.setRoute(Router.RouteType.FORWARD);
                request.setAttribute("errorMessage", e.getMessage());
                router.setPagePath(PagePath.ERROR);
                return router;
            }
            switch (updatingMessage) {
                case "true":
                    sessionUser.setName(name);
                    sessionUser.setPhone(phone);
                    sessionUser.setEmail(email);
                    session.setAttribute("user", sessionUser);
                    break;
                case "incorrect":
                    session.setAttribute(Message.ERROR_MESSAGE,
                            rb.getMessage("message.incorrect_user_data"));
                    break;
                case "false":
                    session.setAttribute(Message.ERROR_MESSAGE,
                            rb.getMessage("message.exist_email") + requestEmail);
                    break;
                default:
                    break;
            }
        } else {
            session.setAttribute(Message.ERROR_MESSAGE,
                    rb.getMessage("message.no_access"));
        }

        return router;
    }
}
