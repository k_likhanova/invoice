package by.training.invoice.command.postimpl;

import by.training.invoice.command.Command;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.InvoiceLogic;
import by.training.invoice.util.LocaleResourceBundle;
import by.training.invoice.util.Message;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.util.RequestParameter;
import by.training.invoice.validator.InputEmptyValidator;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;

/**
 * The type Create invoice command.
 * Created on 20.01.2019.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class CreateInvoiceCommand implements Command {
    /**
     * Field specifies seller id attribute.
     */
    private static final String SELLER_ID_ATTRIBUTE= "id";
    /**
     * Field specifies the array with service ids parameter.
     */
    private static final String SERVICES_PARAMETER= "services";
    /**
     * Field specifies the array with service amounts parameter.
     */
    private static final String SERVICE_AMOUNTS_PARAMETER = "serviceAmounts";
    /**
     * Field specifies object of Invoice logic.
     */
    private final InvoiceLogic logic = new InvoiceLogic();

    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        LocaleResourceBundle rb = (LocaleResourceBundle) session
                                    .getAttribute(Message.RESOURCE_BUNDLE_ATTRIBUTE);

        User sessionUser = (User) session.getAttribute(RequestParameter.USER_ATTRIBUTE);
        String title = request.getParameter(RequestParameter.TITLE_PARAMETER)
                        .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
        String description = request.getParameter(RequestParameter.DESCRIPTION_PARAMETER)
                .replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
        Date paymentDeadline = Date.valueOf(request.getParameter(RequestParameter.PAYMENT_DEADLINE_PARAMETER));
        String customerEmail = request.getParameter(RequestParameter.CUSTOMER_EMAIL_PARAMETER);
        long sellerId = (Long) request.getAttribute(SELLER_ID_ATTRIBUTE);
        String[] services = request.getParameter(SERVICES_PARAMETER).split(",");
        String[] serviceAmount = request.getParameter(SERVICE_AMOUNTS_PARAMETER).split(",");

        if (!InputEmptyValidator.isEmptyInvoiceData(title, customerEmail, sessionUser.getId(),sellerId)
                && services.length != 0) {
            try {
                if(!logic.create(title, description, paymentDeadline, sellerId,
                             customerEmail, sessionUser.getId(), services,serviceAmount)) {
                    session.setAttribute(Message.ERROR_MESSAGE,
                            rb.getMessage("message.no_such_email") + customerEmail);
                }
            } catch (LogicException e) {
                log.error(e);
                request.setAttribute("errorMessage", e.getMessage());
                router.setPagePath(PagePath.ERROR);
                return router;
            }
        } else {
            session.setAttribute(Message.ERROR_MESSAGE,
                                rb.getMessage("message.not_all_info"));
        }

        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath(String.format(PagePath.PROFILE_HTML, sellerId));
        return router;
    }
}
