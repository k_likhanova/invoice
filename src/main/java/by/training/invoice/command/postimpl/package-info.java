/**
 * Provides the classes necessary to
 *          implementation of post user commands
 *          implemented interface Command.
 * Pattern Command is used.
 */
package by.training.invoice.command.postimpl;
