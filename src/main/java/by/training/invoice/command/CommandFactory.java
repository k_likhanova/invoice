package by.training.invoice.command;

import lombok.extern.log4j.Log4j2;

/**
 * Command factory.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class CommandFactory {
    /**
     * The enum Command type.
     */
    public enum  CommandType {
        /**
         * Get command type.
         */
        GET,
        /**
         * Post command type.
         */
        POST;
    }

    /**
     * Private constructor, because all method is static.
     */
    private CommandFactory() { }

    /**
     * Defines what command to create.
     *
     * @param commandType the command type
     * @param command     the name of the command
     * @return the command
     */
    public static Command defineCommand(final String commandType,
                                        final String command) {
        Command current;
        switch (CommandType.valueOf(commandType.toUpperCase())) {
            case GET:
                current = GetCommandType.valueOf(command.toUpperCase()).getCommand();
                break;
            case POST:
                current = PostCommandType.valueOf(command.toUpperCase()).getCommand();
                break;
            default:
                throw new IllegalArgumentException();
        }
        return current;
    }
}
