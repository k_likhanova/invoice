package by.training.invoice.command;
import by.training.invoice.command.getimpl.*;
import by.training.invoice.command.postimpl.LogoutCommand;

/**
 * The enum Get command type.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 */
public enum GetCommandType {
    /**
     * The Invoices list.
     */
    INVOICES_LIST(new InvoicesListCommand()),
    /**
     * Go to the profile page.
     */
    TO_PROFILE(new GoProfileCommand()),
    /**
     * Go to the registration page.
     */
    TO_REGISTRATION(new GoRegistrationCommand()),
    /**
     * Go to the login page.
     */
    TO_LOGIN(new GoLoginCommand()),
    /**
     * Logout.
     */
    LOGOUT(new LogoutCommand()),
    /**
     * Find invoice.
     */
    FIND_INVOICE(new FindInvoiceCommand()),
    /**
     * Search of the user.
     */
    USERS_SEARCH(new UsersSearchCommand()),
    /**
     * Search of the invoices.
     */
    INVOICES_SEARCH(new InvoicesSearchCommand());

    /**
     * Field specifies the object of command.
     */
    private Command command;

    /**
     * Instantiates a new GetCommandType.
     *
     * @param newCommand the new command
     */
    GetCommandType(final Command newCommand) {
        this.command = newCommand;
    }

    /**
     * Gets command.
     *
     * @return the command
     */
    public Command getCommand() {
        return command;
    }

    /**
     * Determines whether contains  command.
     *
     * @param command the name of the command
     * @return the boolean
     */
    public static boolean isContainsCommand(final String command) {
        boolean isContains = false;
        for (GetCommandType element : values()) {
            if (command.equals(element.name())) {
                isContains = true;
            }
        }
        return isContains;
    }
}
