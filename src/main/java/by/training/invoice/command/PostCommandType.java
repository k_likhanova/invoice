package by.training.invoice.command;

import by.training.invoice.command.postimpl.*;

/**
 * The enum Post command type.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 */
public enum PostCommandType {
    /**
     * The Registration.
     */
    REGISTRATION(new RegistrationCommand()),
    /**
     * The Login.
     */
    LOGIN(new LoginCommand()),
    /**
     * The Logout.
     */
    LOGOUT(new LogoutCommand()),
    /**
     * The Change user data.
     */
    CHANGE_USER_DATA(new ChangeUserDataCommand()),
    /**
     * The create service.
     */
    CREATE_SERVICE(new CreateServiceCommand()),
    /**
     * The remove service.
     */
    REMOVE_SERVICE(new RemoveServiceCommand()),
    /**
     * The recharge balance.
     */
    RECHARGE_BALANCE(new RechargeBalanceCommand()),
    /**
     * The create payment.
     */
    PAY(new PayCommand()),
    /**
     * The change locale.
     */
    LOCALE(new LocaleCommand()),
    /**
     * The remove invoice.
     */
    REMOVE_INVOICE(new RemoveInvoiceCommand()),
    /**
     * The create invoice.
     */
    CREATE_INVOICE(new CreateInvoiceCommand()),
    /**
     * The approve invoice.
     */
    APPROVE_INVOICE(new ApproveInvoiceCommand());

    /**
     * Field specifies the object of command.
     */
    private Command command;

    /**
     * Instantiates a new GetCommandType.
     *
     * @param newCommand the new command
     */
    PostCommandType(final Command newCommand) {
        this.command = newCommand;
    }

    /**
     * Gets command.
     *
     * @return the command
     */
    public Command getCommand() {
        return command;
    }

    /**
     * Determines whether contains  command.
     *
     * @param command the name of the command
     * @return the boolean
     */
    public static boolean isContainsCommand(final String command) {
        boolean isContains = false;
        for (PostCommandType element : values()) {
            if (command.equals(element.name())) {
                isContains = true;
            }
        }
        return isContains;
    }
}
