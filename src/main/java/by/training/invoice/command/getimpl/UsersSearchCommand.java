package by.training.invoice.command.getimpl;

import by.training.invoice.command.Command;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.UserLogic;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.util.RequestParameter;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The type Users search command.
 * Created on 18.01.2019
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class UsersSearchCommand implements Command {
    /**
     * Field specifies object of User logic.
     */
    private final UserLogic logic = new UserLogic();

    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        String searchQuery = request.getParameter(RequestParameter.SEARCH_QUERY_PARAMETER);
        if (searchQuery == null) {
            searchQuery = "";
        }
        searchQuery = searchQuery.replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
        List<User> users;
        try {
            users = logic.searchOfUsers(searchQuery);
        } catch (LogicException e) {
            log.error(e);
            request.setAttribute("errorMessage", e.getMessage());
            router.setPagePath(PagePath.ERROR);
            return router;
        }
        request.setAttribute("users", users);
        router.setPagePath(PagePath.USERS_SEARCH_JSP);
        return router;
    }
}
