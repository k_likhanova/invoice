package by.training.invoice.command.getimpl;

import by.training.invoice.command.Command;
import by.training.invoice.entity.impl.Invoice;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.InvoiceLogic;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.util.RequestParameter;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;

/**
 * Invoice list command.
 * Created on 15.01.2019
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class InvoicesListCommand implements Command {
    /**
     * Field specifies object of Service logic.
     */
    private final InvoiceLogic logic = new InvoiceLogic();
    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        User sessionUser = (User) request.getSession().getAttribute(RequestParameter.USER_ATTRIBUTE);
        List<Invoice> invoices;
        try {
            invoices = logic.findAll(sessionUser.getId());
        } catch (LogicException e) {
            log.error(e);
            request.setAttribute("errorMessage", e.getMessage());
            router.setPagePath(PagePath.ERROR);
            return router;
        }
        invoices.sort(Comparator.comparing(Invoice::calculateInvoiceOverdraft).reversed());
        request.setAttribute("invoices", invoices);
        router.setPagePath(PagePath.INVOICES_JSP);
        return router;
    }
}
