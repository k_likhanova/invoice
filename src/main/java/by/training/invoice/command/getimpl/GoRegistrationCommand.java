package by.training.invoice.command.getimpl;

import by.training.invoice.command.Command;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Go to registration page command.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 */
public class GoRegistrationCommand implements Command {
    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        Router router = new Router();
        router.setPagePath(PagePath.REGISTRATION_JSP);
        return router;
    }
}
