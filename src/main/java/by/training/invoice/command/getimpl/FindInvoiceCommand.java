package by.training.invoice.command.getimpl;

import by.training.invoice.command.Command;
import by.training.invoice.entity.impl.Invoice;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.InvoiceLogic;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.util.RequestParameter;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Find invoice command.
 * Created on 14.01.2019
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class FindInvoiceCommand implements Command {
    /**
     * Field specifies object of Service logic.
     */
    private final InvoiceLogic logic = new InvoiceLogic();

    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        Router router = new Router();
        long invoiceId = (Long) request.getAttribute(RequestParameter.INVOICE_ID_ATTRIBUTE);
        User sessionUser = (User) request.getSession().getAttribute(RequestParameter.USER_ATTRIBUTE);
        Invoice invoice;
        try {
            long[] identifications = logic.findSellerAndCustomer(invoiceId);
            if (sessionUser.getId() == identifications[0]
                    || sessionUser.getId() == identifications[1]) {
                invoice = logic.find(invoiceId);
            } else {
                throw new LogicException("This invoice doesn't belong to the user.");
            }
        } catch (LogicException e) {
            log.error(e);
            request.setAttribute("errorMessage", e.getMessage());
            router.setPagePath(PagePath.ERROR);
            return router;
        }
        request.setAttribute("invoice", invoice);
        router.setPagePath(PagePath.INVOICE_JSP);
        return router;
    }
}
