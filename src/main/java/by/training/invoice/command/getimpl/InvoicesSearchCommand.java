package by.training.invoice.command.getimpl;

import by.training.invoice.command.Command;
import by.training.invoice.entity.impl.Invoice;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.InvoiceLogic;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.util.RequestParameter;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The type Invoices search command.
 * Created on 18.01.2019
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class InvoicesSearchCommand implements Command {
    /**
     * Field specifies object of Invoice logic.
     */
    private final InvoiceLogic logic = new InvoiceLogic();

    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        User sessionUser = (User) request.getSession().getAttribute(RequestParameter.USER_ATTRIBUTE);
        String searchQuery = request.getParameter(RequestParameter.SEARCH_QUERY_PARAMETER);
        if (searchQuery == null) {
            searchQuery = "";
        }
        searchQuery = searchQuery.replaceAll(RequestParameter.SCRIPT_PATTERN, RequestParameter.EMPTY);
        List<Invoice> invoices;
        try {
            invoices = logic.searchOfInvoices(searchQuery, sessionUser.getId());
        } catch (LogicException e) {
            log.error(e);
            request.setAttribute("errorMessage", e.getMessage());
            router.setPagePath(PagePath.ERROR);
            return router;
        }
        request.setAttribute("invoices", invoices);
        router.setPagePath(PagePath.INVOICES_JSP);
        return router;
    }
}