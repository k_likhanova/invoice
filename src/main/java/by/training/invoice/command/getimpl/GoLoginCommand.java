package by.training.invoice.command.getimpl;

import by.training.invoice.command.Command;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Go to login page command.
 * Created on 11.01.2019
 *
 * @author Kseniya Likhanova
 */
public class GoLoginCommand implements Command {
    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        Router router = new Router();
        router.setPagePath(PagePath.LOGIN_JSP);
        return router;
    }
}
