package by.training.invoice.command.getimpl;

import by.training.invoice.command.Command;
import by.training.invoice.util.PagePath;
import by.training.invoice.command.Router;
import by.training.invoice.entity.impl.Service;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.LogicException;
import by.training.invoice.logic.UserLogic;
import by.training.invoice.util.RequestParameter;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;

/**
 * Go to the profile command.
 * Created on 29.12.2018.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class GoProfileCommand implements Command {
    /**
     * Field specifies object of User logic.
     */
    private final UserLogic logic = new UserLogic();

    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    @Override
    public Router execute(final HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        User sessionUser = (User) session.getAttribute(RequestParameter.USER_ATTRIBUTE);
        long requestUserId = (Long) request.getAttribute(RequestParameter.USER_ID_ATTRIBUTE);
        User user;
        List<Service> services;
        boolean isSessionUserProfile = sessionUser.getId() == requestUserId;
        try {
            user = logic.findUserInformation(requestUserId);
            if (user.getName() == null) {
                return null;
            }
            services = logic.findServiceInformation(requestUserId);
            if (isSessionUserProfile) {
                BigDecimal overdraft = logic.findUserOverdraft(requestUserId);
                request.setAttribute("overdraft", overdraft);
            } else {
                user.setCurrentBalance(null);
            }
        } catch (LogicException e) {
            log.error(e);
            request.setAttribute("errorMessage", e.getMessage());
            router.setPagePath(PagePath.ERROR);
            return router;
        }
        request.setAttribute("userProfile", user);
        request.setAttribute("userServices", services);
        router.setPagePath(PagePath.PROFILE_JSP);
        return router;
    }
}
