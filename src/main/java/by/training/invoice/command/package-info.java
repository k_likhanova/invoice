/**
 * Provides the classes necessary to
 *          definition and implementation of user commands.
 * Pattern Command is used.
 */
package by.training.invoice.command;
