package by.training.invoice.command;

/**
 * The class Router is intended for definition
 *      of a path to the page and a way of transition to it
 * Created on 29.12.2018.
 *
 * @author Kseniya Likhanova
 */
public class Router {
    /**
     * The enum Route type.
     */
    public enum RouteType {
        /**
         * Forward route type.
         */
        FORWARD, /**
         * Redirect route type.
         */
        REDIRECT
    }

    /**
     * Field specifies the page path.
     */
    private String pagePath;
    /**
     * Field specifies way of transition to the page.
     * By default forward
     */
    private RouteType route = RouteType.FORWARD;

    /**
     * Sets page path.
     *
     * @param newPagePath the page path
     */
    public void setPagePath(final String newPagePath) {
        this.pagePath = newPagePath;
    }

    /**
     * Sets route.
     *
     * @param newRoute the new route
     */
    public void setRoute(final RouteType newRoute) {
        if (newRoute == null) {
            this.route = RouteType.FORWARD;
        } else {
            this.route = newRoute;
        }
    }

    /**
     * Gets page path.
     *
     * @return the page path
     */
    public String getPagePath() {
        return pagePath;
    }

    /**
     * Gets route.
     *
     * @return the route
     */
    public RouteType getRoute() {
        return route;
    }
}
