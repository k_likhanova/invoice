package by.training.invoice.command;

import javax.servlet.http.HttpServletRequest;

/**
 * The interface Command.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 */
public interface Command {
    /**
     * Executes command.
     *
     * @param request from jsp
     * @return the router
     */
    Router execute(HttpServletRequest request);

}
