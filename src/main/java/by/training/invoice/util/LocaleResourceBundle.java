package by.training.invoice.util;

import java.io.Serializable;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * The enum Locale resource bundle.
 * Created on 11.01.2019
 *
 * @author Kseniya Likhanova
 */
public enum LocaleResourceBundle implements Serializable {
    /**
     * Locale with english language.
     */
    EN(ResourceBundle.getBundle("locale", new Locale("en", "EN"))),
    /**
     * Locale with russian language.
     */
    RU(ResourceBundle.getBundle("locale", new Locale("ru", "RU"))),
    /**
     * Locale with belorussian language.
     */
    BE(ResourceBundle.getBundle("locale", new Locale("be", "BY")));

    /**
     * The field specifies the object of ResourceBundle.
     */
    private ResourceBundle bundle;

    /**
     * Instantiates a new LocaleResourceBundle.
     *
     * @param newBundle the current resource bundle
     */
    LocaleResourceBundle(final ResourceBundle newBundle) {
        this.bundle = newBundle;
    }

    /**
     * Gets message.
     *
     * @param key the key
     * @return the message
     */
    public String getMessage(final String key) {
        return bundle.getString(key);
    }

    /**
     * Gets locale.
     *
     * @return the locale
     */
    public Locale getLocale() {
        return bundle.getLocale();
    }

}
