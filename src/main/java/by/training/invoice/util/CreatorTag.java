package by.training.invoice.util;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * The type Footer tag.
 * Created on 26.01.2019
 *
 * @author Kseniya Likhanova
 */
public class CreatorTag extends SimpleTagSupport {
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        out.println("Created by K. Likhanova in 2019");
    }
}