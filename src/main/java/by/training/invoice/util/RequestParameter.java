package by.training.invoice.util;

/**
 * The class contains the request parameters and attributes.
 * Created on 24.01.2019
 *
 * @author Kseniya Likhanova
 */
public class RequestParameter {
    /**
     * Regular expression for script.
     */
    public static final String SCRIPT_PATTERN = "<\\/?script>";
    /**
     * Field specifies empty string.
     */
    public static final String EMPTY = "";

    /**
     * Field specifies user attribute from session.
     */
    public static final String USER_ATTRIBUTE = "user";
    /**
     * Field specifies seller id parameter.
     */
    public static final String SELLER_ID_PARAMETER = "sellerId";
    /**
     * Field specifies email parameter.
     */
    public static final String EMAIL_PARAMETER = "email";
    /**
     * Field specifies password parameter.
     */
    public static final String PASSWORD_PARAMETER = "password";
    /**
     * Field specifies name parameter.
     */
    public static final String NAME_PARAMETER = "name";
    /**
     * Field specifies phone parameter.
     */
    public static final String PHONE_PARAMETER = "phone";
    /**
     * Field specifies title parameter.
     */
    public static final String TITLE_PARAMETER = "title";
    /**
     * Field specifies description parameter.
     */
    public static final String DESCRIPTION_PARAMETER = "description";
    /**
     * Field specifies customer email parameter.
     */
    public static final String CUSTOMER_EMAIL_PARAMETER = "customerEmail";
    /**
     * Field specifies deadline date for full payment parameter.
     */
    public static final String PAYMENT_DEADLINE_PARAMETER = "paymentDeadline";
    /**
     * Field specifies service price parameter.
     */
    public static final String PRICE_PARAMETER = "price";
    /**
     * Field specifies payment parameter.
     */
    public static final String PAYMENT_PARAMETER = "payment";
    /**
     * Field specifies customer id parameter.
     */
    public static final String CUSTOMER_ID_PARAMETER = "customerId";
    /**
     * Field specifies money amount parameter.
     */
    public static final String MONEY_AMOUNT_PARAMETER = "moneyAmount";
    /**
     * Field specifies service id parameter.
     */
    public static final String SERVICE_ID_PARAMETER = "serviceId";
    /**
     * Field specifies invoice id parameter.
     */
    public static final String INVOICE_ID_PARAMETER = "invoiceId";
    /**
     * Field specifies invoice id attribute.
     */
    public static final String INVOICE_ID_ATTRIBUTE = "id";
    /**
     * Field specifies user profile id attribute.
     */
    public static final String USER_ID_ATTRIBUTE= "id";
    /**
     * Field specifies search query parameter.
     */
    public static final String SEARCH_QUERY_PARAMETER = "searchQuery";

    /**
     * Private constructor, because all method is static.
     */
    private RequestParameter() {
    }
}
