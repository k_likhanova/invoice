package by.training.invoice.util;

/**
 * The type Message.
 * Created on 22.01.2019
 *
 * @author Kseniya Likhanova
 */
public class Message {
    /**
     * Field specifies session resource bundle attribute.
     */
    public static final String RESOURCE_BUNDLE_ATTRIBUTE = "rb";
    /**
     * Field specifies session error message attribute.
     */
    public static final String ERROR_MESSAGE = "errorMessage";
    /**
     * Field specifies session error login message attribute.
     */
    public static final String ERROR_LOGIN_MESSAGE = "errorLoginMessage";
    /**
     * Field specifies session error registration message attribute.
     */
    public static final String ERROR_REGISTRATION_MESSAGE = "errorRegistrationMessage";
    /**
     * Field specifies session successful registration message attribute.
     */
    public static final String SUCCESSFUL_REGISTRATION_MESSAGE = "successfulRegistrationMessage";

    /**
     * Private constructor, because all method is static.
     */
    private Message() {
    }

}
