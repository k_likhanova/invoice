package by.training.invoice.util;

/**
 * The type Page path.
 * Created on 29.12.2018.
 *
 * @author Kseniya Likhanova
 */
public class PagePath {
    /**
     * The constant specifies the login page path for redirect.
     */
    public static final String LOGIN_HTML = "/login.html";
    /**
     * The constant specifies the registration page path for redirect.
     */
    public static final String REGISTRATION_HTML = "/registration.html";
    /**
     * The constant specifies the profile page path for redirect.
     */
    public static final String PROFILE_HTML = "/profile/%s.html";
    /**
     * The constant specifies the invoice page path for redirect.
     */
    public static final String INVOICE_HTML = "/invoice/%s.html";
    /**
     * The constant specifies the invoice page path for redirect.
     */
    public static final String INVOICES_HTML = "/invoices.html";

    /**
     * The constant specifies the error page path for redirect.
     */
    public static final String ERROR = "/jsp/error/error.jsp";
    /**
     * The constant specifies the registration page path for forward.
     */
    public static final String REGISTRATION_JSP = "/jsp/registration.jsp";
    /**
     * The constant specifies the login page path for forward.
     */
    public static final String LOGIN_JSP = "/jsp/login.jsp";
    /**
     * The constant specifies the profile page path for forward.
     */
    public static final String PROFILE_JSP = "/jsp/userProfile.jsp";
    /**
     * The constant specifies the invoice page path for forward.
     */
    public static final String INVOICE_JSP = "/jsp/invoice.jsp";
    /**
     * The constant specifies the invoice page path for forward.
     */
    public static final String INVOICES_JSP = "/jsp/invoices.jsp";
    /**
     * The constant specifies the users search page path for forward.
     */
    public static final String USERS_SEARCH_JSP = "/jsp/usersSearch.jsp";

    /**
     * Private constructor, because all method is static.
     */
    private PagePath() {
    }
}
