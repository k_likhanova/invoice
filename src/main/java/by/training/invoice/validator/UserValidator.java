package by.training.invoice.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type User validator.
 * Created on 29.12.2018.
 *
 * @author Kseniya Likhanova
 */
public class UserValidator {
    /**
     * Regular expression for email.
     */
    private static final String EMAIL_PATTERN =
            "^[\\w.-]+@[a-zA-Z\\d.-]+\\.[a-z]{2,6}$";
    /**
     * Regular expression for password.
     */
    private static final String PASSWORD_PATTERN =
            "(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z])\\w{6,20}";
    /**
     * Regular expression for phone.
     */
    private static final String PHONE_PATTERN =
            "^\\+375(29|25|33|44)\\d{7}$";
    /**
     * Regular expression for name.
     */
    private static final String NAME_PATTERN =
            "^.{1,100}$";

    /**
     * Private constructor, because all method is static.
     */
    private UserValidator() { }

    /**
     * Defines whether the email is valid.
     *
     * @param email the email
     * @return the boolean
     */
    public static boolean isValidEmail(final String email) {
        Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
        Matcher emailMatcher = emailPattern.matcher(email);
        return emailMatcher.matches();
    }

    /**
     * Defines whether the password is valid.
     *
     * @param password the password
     * @return the boolean
     */
    public static boolean isValidPassword(final String password) {
        Pattern passwordPattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher passwordMatcher = passwordPattern.matcher(password);
        return passwordMatcher.matches();
    }

    /**
     * Defines whether the phone is valid.
     *
     * @param phone the phone
     * @return the boolean
     */
    public static boolean isValidPhone(final String phone) {
        Pattern phonePattern = Pattern.compile(PHONE_PATTERN);
        Matcher phoneMatcher = phonePattern.matcher(phone);
        return phoneMatcher.matches();
    }

    /**
     * Defines whether the name is valid.
     *
     * @param name the name
     * @return the boolean
     */
    public static boolean isValidName(final String name) {
        Pattern namePattern = Pattern.compile(NAME_PATTERN);
        Matcher nameMatcher = namePattern.matcher(name);
        return nameMatcher.matches();
    }

    /**
     * Defines whether the login data valid.
     *
     * @param email    the email
     * @param password the password
     * @return the boolean
     */
    public static boolean isValidLoginData(final String email,
                                           final String password) {
        return isValidEmail(email)
                && isValidPassword(password);
    }

    /**
     * Defines whether the user data are valid.
     *
     * @param email the email
     * @param phone the phone
     * @param name  the name
     * @return the boolean
     */
    public static boolean isValidUserData(final String email,
                                          final String phone,
                                          final String name) {
        return isValidEmail(email) && isValidPhone(phone) && isValidName(name);
    }

    /**
     * Defines whether the user data is changed.
     *
     * @param email the email
     * @param name the name
     * @param phone the phone
     * @param newEmail the new email
     * @param newName the new name
     * @param newPhone the new phone
     * @return the boolean
     */
    public static boolean isChangedData(final String email, final String name,
                                        final String phone, final String newEmail,
                                        final String newName, final String newPhone) {
        return !email.equals(newEmail) || !name.equals(newName)
                || !phone.equals(newPhone);
    }
}
