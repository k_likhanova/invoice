package by.training.invoice.validator;

import by.training.invoice.entity.impl.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type Util validator.
 * Created on 06.01.2019
 *
 * @author Kseniya Likhanova
 */
public class UtilValidator {
    /**
     * Regular expression for price.
     */
    private static final String PRICE_PATTERN =
            "^[1-9]\\d*((,\\d+)*(\\.\\d+)?)|((\\.\\d+)*(,\\d+)?)$";

    /**
     * Private constructor, because all method is static.
     */
    private UtilValidator() { }

    /**
     * Defines whether the price is valid.
     *
     * @param price the price
     * @return the boolean
     */
    public static boolean isValidPrice(final String price) {
        Pattern pricePattern = Pattern.compile(PRICE_PATTERN);
        Matcher priceMatcher = pricePattern.matcher(price);
        return priceMatcher.matches();
    }
    /**
     * Defines whether the user is authenticated.
     *
     * @param sessionUser the user from the session
     * @return boolean
     */
    public static boolean isAuthenticatedUser(final User sessionUser) {
        return sessionUser != null;
    }

    /**
     * Defines whether the page is for not the authenticated user is.
     *
     * @param uri the request uri
     * @return boolean
     */
    public static boolean isPageForNotAuthenticatedUser(final String uri) {
        return uri.contains("login") || uri.contains("registration");
    }

}
