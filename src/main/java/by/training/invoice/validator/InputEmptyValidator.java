package by.training.invoice.validator;

/**
 * The type input empty validator.
 * Created on 05.01.2019.
 *
 * @author Kseniya Likhanova
 */
public class InputEmptyValidator {

    /**
     * Private constructor, because all method is static.
     */
    private InputEmptyValidator() { }

    /**
     * Defines whether the login data are not empty.
     *
     * @param email    the email
     * @param password the password
     * @return boolean boolean
     */
    public static boolean isEmptyLoginData(final String email,
                                           final String password) {
        return email.isEmpty()
                || password.isEmpty();
    }

    /**
     * Defines whether the user data are not empty.
     *
     * @param name  the name
     * @param phone the phone
     * @return boolean boolean
     */
    public static boolean isEmptyUserData(final String name,
                                          final String phone) {
        return name.isEmpty()
                || phone.isEmpty();
    }

    /**
     * Defines whether the all user data are empty.
     *
     * @param email the email
     * @param name  the name
     * @param phone the phone
     * @return boolean boolean
     */
    public static boolean isEmptyAllUserData(final String email,
                                             final String name,
                                             final String phone) {
        return email.isEmpty() && name.isEmpty() && phone.isEmpty();
    }

    /**
     * Defines whether the service data are not empty.
     *
     * @param title the title
     * @param price the price
     * @return boolean boolean
     */
    public static boolean isEmptyServiceData(final String title,
                                             final String price) {
        return title.isEmpty() || price.isEmpty();
    }

    /**
     * Defines whether the invoice data are not empty.
     *
     * @param title the title
     * @param customerEmail the email pf the customer
     * @param sessionUserId the id of the user from session
     * @param sellerId the seller id
     * @return boolean boolean
     */
    public static boolean isEmptyInvoiceData(final String title, final String customerEmail,
                                             final long sessionUserId, final long sellerId) {
        boolean isEmpty = title.isEmpty();
        if(sessionUserId == sellerId) {
            isEmpty = isEmpty || customerEmail.isEmpty();
        }
        return isEmpty;
    }
}
