package by.training.invoice.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * The abstract type Entity.
 * Created on 22.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public abstract class Entity implements Serializable {
    /**
     * Field id specifies the id of Entity.
     */
    private long id;

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param newId the id
     */
    public void setId(final long newId) {
        this.id = newId;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Long.compare(id, ((Entity) obj).id) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
