package by.training.invoice.entity.impl;

import by.training.invoice.entity.Entity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * The type User.
 * Created on 22.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class User extends Entity {
    /**
     * Field email specifies the email.
     */
    private String email;
    /**
     * Field password specifies the password.
     */
    private String password;
    /**
     * Field currentBalance specifies the current balance.
     */
    private BigDecimal currentBalance;
    /**
     * Field specifies the name of the user.
     */
    private String name;
    /**
     * Field phone specifies the phone.
     */
    private String phone;

    /**
     * Instantiates a new User.
     *
     * @param newId the id
     * @param newEmail the email
     * @param newName the name
     * @param newPhone the phone
     */
    public User(final long newId, final String newEmail, final String newName, final String newPhone) {
        super.setId(newId);
        this.email = newEmail;
        this.name = newName;
        this.phone = newPhone;
    }


}
