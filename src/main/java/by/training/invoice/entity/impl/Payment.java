package by.training.invoice.entity.impl;

import by.training.invoice.entity.Entity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

/**
 * The type Payment.
 * Created on 22.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
public class Payment extends Entity {
    /**
     * Field payment specifies how many money was paid.
     */
    private BigDecimal payment;
    /**
     * Field date specifies when the payment was made.
     */
    private Date date;
    /**
     * Field date specifies when the payment was made.
     */
    private Time time;
}
