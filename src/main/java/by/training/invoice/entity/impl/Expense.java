package by.training.invoice.entity.impl;

import by.training.invoice.entity.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * The type Expense.
 * Created on 22.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class Expense extends Entity {
    /**
     * Field specifies what service belongs to the expense.
     */
    private Service service;
    /**
     * Field specifies amount of services in expense.
     */
    private int serviceAmount;
    /**
     * Field specifies the total price of the expense.
     */
    private BigDecimal totalPrice;
}
