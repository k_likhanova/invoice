package by.training.invoice.entity.impl;

import by.training.invoice.entity.Entity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * The type Service.
 * Created on 22.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Service extends Entity {
    /**
     * Field specifies the title.
     */
    private String title;
    /**
     * Field specifies the description.
     */
    private String description;
    /**
     * Field specifies the price.
     */
    private BigDecimal price;
    /**
     * Field specifies to what seller the service belongs.
     */
    private long sellerId;
    /**
     * Field defines whether the service is archival.
     */
    private boolean isArchival;

    /**
     * Instantiates a new Service
     *
     * @param newId the id
     * @param newTitle the title
     * @param newDescription the description
     * @param newPrice the price
     * @param newSellerId the seller id
     */
    public Service(final long newId, final String newTitle,
                   final String newDescription, final BigDecimal newPrice,
                   final long newSellerId) {
        super.setId(newId);
        this.title = newTitle;
        this.description = newDescription;
        this.price = newPrice;
        this.sellerId = newSellerId;
    }
}
