package by.training.invoice.entity.impl;

import by.training.invoice.entity.Entity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * The type Invoice.
 * Created on 22.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Invoice extends Entity {
    /**
     * Field title specifies the title.
     */
    private String title;
    /**
     * Field description specifies the response of the seller
*                                  to request of the customer.
     */
    private String description;
    /**
     * Field createdDate specifies when the invoice was created.
     */
    private Date createdDate;
    /**
     * Field paymentDeadline specifies up to what date the customer has to pay.
     */
    private Date paymentDeadline;
    /**
     * Field specifies the seller.
     */
    private User seller;
    /**
     * Field customer specifies the customer.
     */
    private User customer;
    /**
     * Field specifies what expenses belong to the invoice.
     */
    private List<Expense> expenses;
    /**
     * Field specifies what payments belong to the invoice.
     */
    private List<Payment> payments;
    /**
     * Field defines whether the invoice is archival for customer.
     */
    private boolean isArchivalForCustomer;
    /**
     * Field defines whether the invoice is archival for seller.
     */
    private boolean isArchivalForSeller;
    /**
     * Field defines whether the invoice is approved.
     */
    private boolean isApproved;
    /**
     * Field specifies the total price of the Invoice.
     */
    private BigDecimal totalPrice;
    /**
     * Field specifies the payments sum of the Invoice.
     */
    private BigDecimal paymentsSum;

    /**
     * Instantiates a new Invoice.
     *
     * @param newId              the new id
     * @param newTitle           the new title
     * @param newDescription     the new description
     * @param newPaymentDeadline the new payment deadline
     * @param newSeller          the new seller
     * @param newCustomer        the new customer
     * @param newIsApproved      the new is approved
     */
    public Invoice(final long newId, final String newTitle,
                   final String newDescription, final Date newPaymentDeadline,
                   final User newSeller, final User newCustomer, final boolean newIsApproved) {
        super.setId(newId);
        this.title = newTitle;
        this.description = newDescription;
        this.paymentDeadline = newPaymentDeadline;
        this.seller = newSeller;
        this.customer = newCustomer;
        this.expenses = new LinkedList<>();
        this.payments = new LinkedList<>();
        this.isApproved = newIsApproved;
    }

    /**
     * Add expense.
     *
     * @param expense the expense
     */
    public void addExpense(final Expense expense) {
        expenses.add(expense);
    }

    /**
     * Remove expense.
     *
     * @param expense the expense
     */
    public void removeExpense(final Expense expense) {
        expenses.remove(expense);
    }

    /**
     * Gets expense.
     *
     * @param index the index
     * @return the expense
     */
    public Expense getExpense(final int index) {
        return expenses.get(index);
    }

    /**
     * Add payment.
     *
     * @param payment the payment
     */
    public void addPayment(final Payment payment) {
        payments.add(payment);
    }

    /**
     * Remove payment.
     *
     * @param payment the payment
     */
    public void removePayment(final Payment payment) {
        payments.remove(payment);
    }

    /**
     * Gets payment.
     *
     * @param index the index
     * @return the payment
     */
    public Payment getPayment(final int index) {
        return payments.get(index);
    }

    /**
     * Calculate invoice overdraft.
     *
     * @return the invoice overdraft
     */
    public BigDecimal calculateInvoiceOverdraft() {
        BigDecimal overdraft;
        if(paymentsSum != null) {
            overdraft = totalPrice
                    .subtract(paymentsSum);
        } else {
            overdraft = totalPrice;
        }
        return overdraft;
    }
}
