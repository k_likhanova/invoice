package by.training.invoice.controller;

import by.training.invoice.command.Command;
import by.training.invoice.command.CommandFactory;
import by.training.invoice.command.GetCommandType;
import by.training.invoice.command.PostCommandType;
import by.training.invoice.connection.ConnectionPool;
import by.training.invoice.exception.DbConnectionException;
import by.training.invoice.command.Router;
import by.training.invoice.util.PagePath;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

/**
 * The type Controller.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class Controller extends HttpServlet {
    /**
     * The constant specifies the request command.
     */
    private static final String COMMAND = "command";

    @Override
    public void init() {
        try {
            ConnectionPool.getInstance().initialize();
        } catch (DbConnectionException e) {
            log.fatal("It is impossible to initialize application", e);
            destroy();
        }
        Locale.setDefault(new Locale("en", "EN"));
    }

    @Override
    public void destroy() {
        ConnectionPool.getInstance().destroy();
    }


    @Override
    public void doGet(final HttpServletRequest request,
                      final HttpServletResponse response)
                      throws ServletException, IOException {
        String commandName = request.getParameter(COMMAND);
        Router router = null;
        if (commandName == null) {
            commandName = (String) request.getAttribute(COMMAND);
        }
        if (commandName != null
                && GetCommandType.isContainsCommand(commandName.toUpperCase())) {
            Command command = CommandFactory.defineCommand(
                                CommandFactory.CommandType.GET.name().toLowerCase(), commandName);
            router = command.execute(request);
        } else if (commandName != null) {
            router = new Router();
            String errorMessage = "There is no such command.";
            log.error(errorMessage);
            request.setAttribute("errorMessage", errorMessage);
            router.setPagePath(PagePath.ERROR);
        }
        if (router != null) {
            getServletContext().getRequestDispatcher(router.getPagePath())
                                .forward(request, response);
        } else {
            response.sendError(404, "page not found");
        }

    }

    @Override
    public void doPost(final HttpServletRequest request,
                       final HttpServletResponse response)
                       throws ServletException, IOException {
        String commandName = request.getParameter(COMMAND);
        Router router = null;
        if (commandName != null
                && PostCommandType.isContainsCommand(commandName.toUpperCase())) {
            Command command = CommandFactory.defineCommand(
                                CommandFactory.CommandType.POST.name().toLowerCase(), commandName);
            router = command.execute(request);
        } else if (commandName != null) {
            router = new Router();
            String errorMessage = "There is no such command.";
            request.setAttribute("errorMessage", errorMessage);
            log.error(errorMessage);
            router.setPagePath(PagePath.ERROR);
        }
        if (router != null) {
            if (router.getRoute() == Router.RouteType.FORWARD) {
                getServletContext().getRequestDispatcher(router.getPagePath())
                        .forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + router.getPagePath());
            }
        } else {
            response.sendError(404, "page not found");
        }
    }
}
