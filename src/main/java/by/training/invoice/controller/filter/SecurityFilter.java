package by.training.invoice.controller.filter;

import by.training.invoice.util.PagePath;
import by.training.invoice.entity.impl.User;
import by.training.invoice.validator.UtilValidator;
import lombok.extern.log4j.Log4j2;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The type Security filter is intended for definition
 *              whether the page is available to the user.
 * Created on 12.01.2019
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain filterChain)
                         throws IOException, ServletException {
        if (request instanceof HttpServletRequest
                && response instanceof HttpServletResponse) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            HttpSession session = httpRequest.getSession(false);
            boolean isAuthenticatedUser = session != null
                                            && UtilValidator.isAuthenticatedUser(
                                                (User) session.getAttribute("user"));
            String uri = httpRequest.getRequestURI();
            if ((isAuthenticatedUser
                    && !UtilValidator.isPageForNotAuthenticatedUser(uri))
                || (!isAuthenticatedUser
                    && UtilValidator.isPageForNotAuthenticatedUser(uri))) {
                filterChain.doFilter(request, response);
            } else if (isAuthenticatedUser) {
                httpResponse.sendRedirect(PagePath.PROFILE_HTML);
            } else {
                httpResponse.sendRedirect(PagePath.LOGIN_HTML);
            }
        } else {
            String errorMessage = "It is impossible to use HTTP filter";
            log.error(errorMessage);
            request.setAttribute("errorMessage",errorMessage);
            request.getRequestDispatcher(PagePath.ERROR)
                    .forward(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
