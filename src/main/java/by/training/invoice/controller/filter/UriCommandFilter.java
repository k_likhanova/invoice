package by.training.invoice.controller.filter;

import by.training.invoice.util.PagePath;
import lombok.extern.log4j.Log4j2;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The type Uri command filter.
 * Created on 11.01.2019
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class UriCommandFilter implements Filter {
    /**
     * Field associates the uri with the command name.
     */
    private static Map<String, String> commands = new ConcurrentHashMap<>();

    static {
        commands.put("/", "login");
        commands.put("/login", "to_login");
        commands.put("/registration", "to_registration");
        commands.put("/profile", "to_profile");
        commands.put("/invoices", "invoices_list");
        commands.put("/invoice", "find_invoice");
        commands.put("/usersSearch", "users_search");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            String contextPath = httpRequest.getContextPath();
            String uri = httpRequest.getRequestURI();
            int beginCommand = contextPath.length();
            int endCommand = uri.lastIndexOf('.');
            String commandName;
            if (endCommand >= 0) {
                commandName = uri.substring(beginCommand, endCommand);
            } else {
                commandName = uri.substring(beginCommand);
            }
            endCommand = commandName.lastIndexOf('/');
            if(endCommand != 0) {
                httpRequest.setAttribute("id",
                        Long.parseLong(commandName.substring(endCommand + 1)));
                commandName = commandName.substring(0, endCommand);
            }
            httpRequest.setAttribute("command", commands.get(commandName));
            chain.doFilter(request, response);
        } else {
            String errorMessage = "It is impossible to use HTTP filter";
            log.error(errorMessage);
            request.setAttribute("errorMessage",errorMessage);
            request.getServletContext().getRequestDispatcher(PagePath.ERROR)
                    .forward(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}