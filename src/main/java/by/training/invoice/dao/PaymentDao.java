package by.training.invoice.dao;

import by.training.invoice.entity.impl.Payment;
import by.training.invoice.exception.DaoException;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public interface PaymentDao extends Dao<Payment> {
    /**
     * Add payment.
     *
     * @param payment the payment
     * @param invoiceId to what invoice the payment belongs
     * @return the id of payment
     * @throws DaoException the dao exception
     */
    long create(final BigDecimal payment,
                       final long invoiceId) throws DaoException;

    /**
     * Find all payments in invoice.
     *
     * @param invoiceId the invoice id
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Payment> findAll(final long invoiceId) throws DaoException;

    /**
     * Find all entities.
     *
     * @return the list
     */
    @Override
    default List<Payment> findAll() {
        return new LinkedList<>();
    }

    /**
     * Find entity by id.
     *
     * @param id the id of entity
     * @return the entity
     */
    default Payment find(long id) {
        return new Payment();
    }

    /**
     * Add entity.
     *
     * @param entity the entity
     * @return the id of entity
     */
    default long create(Payment entity) {
        return 0;
    }

}
