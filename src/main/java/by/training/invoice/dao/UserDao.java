package by.training.invoice.dao;

import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.DaoException;

import java.math.BigDecimal;
import java.util.List;

public interface UserDao extends Dao<User> {
    /**
     * Update entity.
     *
     * @param user the new user data
     */
    void update(final User user) throws DaoException;

    /**
     * Finds user id by email.
     *
     * @param email the email
     * @return the user id
     * @throws DaoException the dao exception
     */
    long findUserIdByEmail(final String email) throws DaoException;

    /**
     * Find user by email user.
     *
     * @param email    the email
     * @param password the password
     * @return the user
     * @throws DaoException the dao exception
     */
    User findUserByEmail(final String email,
                                final String password) throws DaoException;

    /**
     * Finds current balance of the user.
     *
     * @param userId the id of user
     * @throws DaoException the dao exception
     */
    BigDecimal findCurrentBalance(final long userId) throws DaoException;

    /**
     * Recharges current balance on the moneyAmount of the user with id=userId.
     *
     * @param currentBalance the current balance
     * @param moneyAmount the amount of money for recharging of the balance
     * @param userId the id of user
     * @return the new current balance
     * @throws DaoException the dao exception
     */
    BigDecimal rechargeBalance(final BigDecimal currentBalance,
                                      final BigDecimal moneyAmount,
                                      final long userId) throws DaoException;

    /**
     * Pay invoice.
     *
     * @param currentBalance the current balance
     * @param payment the amount of money for payment
     * @param userId the id of user
     * @throws DaoException the dao exception
     */
    void pay(final BigDecimal currentBalance, final BigDecimal payment,
                    final long userId) throws DaoException;

    /**
     * Find invoices summary of the customer.
     *
     * @param customerId the customer id of the invoices
     * @return the invoices summary
     * @throws DaoException the dao exception
     */
    BigDecimal findInvoicesSummary(final long customerId) throws DaoException;

    /**
     * Find payments summary of the customer.
     *
     * @param customerId the customer id of the invoices
     * @return the payments summary
     * @throws DaoException the dao exception
     */
    BigDecimal findPaymentsSummary(final long customerId) throws DaoException;

    /**
     * Search of the users which contains pattern string in name or email.
     *
     * @param searchQuery the search query
     * @return the users
     * @throws DaoException the dao exception
     */
    List<User> searchOfUsers(final String searchQuery) throws DaoException;

}
