package by.training.invoice.dao;

import by.training.invoice.entity.impl.Service;
import by.training.invoice.exception.DaoException;

import java.util.List;

public interface ServiceDao extends Dao<Service> {
    /**
     * Find all services of the user by user id.
     *
     * @param userId the user id
     * @return the services list
     * @throws DaoException the dao exception
     */
    List<Service> findUserServices(final long userId) throws DaoException;

    /**
     * Archives service by id.
     *
     * @param id the id of service
     */
    void remove(final long id) throws DaoException;

}
