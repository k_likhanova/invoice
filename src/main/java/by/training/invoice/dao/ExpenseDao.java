package by.training.invoice.dao;

import by.training.invoice.entity.impl.Expense;
import by.training.invoice.exception.DaoException;

import java.util.LinkedList;
import java.util.List;

public interface ExpenseDao extends Dao<Expense> {

    /**
     * Find all services in invoice.
     *
     * @param invoiceId the invoice id
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Expense> findAll(final long invoiceId) throws DaoException;

    /**
     * Add expense.
     *
     * @param invoiceId     to what invoice the service belongs.
     * @param serviceId     the service id
     * @param serviceAmount the amount of service
     * @return the id of expense
     * @throws DaoException the dao exception
     */
    long create(final long invoiceId, final long serviceId,
                       final int serviceAmount) throws DaoException;

    /**
     * Find all entities.
     *
     * @return the list
     */
    @Override
    default List<Expense> findAll() {
        return new LinkedList<>();
    }

    /**
     * Find entity by id.
     *
     * @param id the id of entity
     * @return the entity
     */
    default Expense find(long id) {
        return new Expense();
    }

    /**
     * Add entity.
     *
     * @param entity the entity
     * @return the id of entity
     */
    default long create(Expense entity) {
        return 0;
    }


}
