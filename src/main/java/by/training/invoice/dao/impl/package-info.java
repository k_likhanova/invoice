/**
 * Provides the classes necessary to realization
 *           of interaction with the database
 *           inherited from AbstractDao.
 */
package by.training.invoice.dao.impl;
