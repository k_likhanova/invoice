package by.training.invoice.dao.impl;

import by.training.invoice.connection.ProxyConnection;
import by.training.invoice.dao.AbstractDao;
import by.training.invoice.dao.InvoiceDao;
import by.training.invoice.entity.impl.Invoice;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.DaoException;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.math.BigDecimal;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * The type Invoice dao.
 * Created on 05.01.2018.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
@NoArgsConstructor
public class InvoiceDaoImpl extends AbstractDao implements InvoiceDao {
    /**
     * Constant field specifies the query on find all invoices for the seller.
     */
    private static final String FIND_ALL =
            "SELECT invoice.id, invoice.title, SUM(price*amount) AS total_price, is_approved, "
                    + "customer_id, customer.name AS customer_name, "
                    + "invoice.seller_id, seller.name AS seller_name "
          + "FROM invoice JOIN expense ON invoice.id=expense.invoice_id "
                       + "JOIN service ON service_id=service.id "
                       + "JOIN user seller ON invoice.seller_id=seller.id "
                       + "JOIN user customer ON customer_id=customer.id "
          + "WHERE (invoice.seller_id=? AND is_archival_for_seller=0) "
            + "OR (invoice.customer_id=?  AND is_archival_for_customer=0 AND is_approved=1) "
          + "GROUP BY invoice.id";
    /**
     * Constant field specifies the query on add invoice.
     */
    private static final String ADD_INVOICE = "INSERT INTO invoice "
            + "(title,description,payment_deadline,customer_id,seller_id, is_approved) "
            + "values(?,?,?,?,?,?)";
    /**
     * Constant field specifies the query on archive invoice for seller.
     */
    private static final String ARCHIVE_FOR_SELLER =
            "UPDATE invoice SET is_archival_for_seller=1 WHERE id=?";
    /**
     * Constant field specifies the query on archive invoice for customer.
     */
    private static final String ARCHIVE_FOR_CUSTOMER =
            "UPDATE invoice SET is_archival_for_customer=1 WHERE id=?";
    /**
     * Constant field specifies the query on approvement invoice.
     */
    private static final String APPROVE_INVOICE =
            "UPDATE invoice SET is_approved=1 WHERE id=?";
    /**
     * Constant field specifies the query on find invoice.
     */
    private static final String FIND_INVOICE =
            "SELECT invoice.*, seller.id AS seller_id, seller.name AS seller_name, "
                + "seller.email AS seller_email, seller.phone AS seller_phone, "
                + "customer.id AS customer_id, customer.name AS customer_name, "
                + "customer.email AS customer_email, customer.phone AS customer_phone "
          + "FROM invoice JOIN user seller ON invoice.seller_id=seller.id "
                       + "JOIN user customer ON customer_id=customer.id "
          + "WHERE invoice.id=?";
    /**
     * Constant field specifies the query on update invoice.
     */
    private static final String FIND_SELLER_AND_CUSTOMER =
            "SELECT seller_id, customer_id FROM invoice WHERE id=?";
    /**
     * Constant field specifies the query on find total price of the invoice.
     */
    private static final String FIND_INVOICE_PRICE =
            "SELECT SUM(price*amount) AS total_price "
            + "FROM invoice JOIN expense ON invoice.id=expense.invoice_id "
                         + "JOIN service ON service_id=service.id "
            + "WHERE invoice.id=?";
    /**
     * Constant field specifies the query on find sum of all the invoice payments.
     */
    private static final String FIND_PAYMENTS_SUM =
            "SELECT SUM(payment) AS sum "
            + "FROM invoice JOIN payment ON invoice.id=payment.invoice_id "
            + "WHERE invoice.id=?";
    /**
     * Constant field specifies the query on find sum of all the payments
     *                                              of the user invoices.
     */
    private static final String FIND_PAYMENTS_SUM_FOR_USER =
            "SELECT SUM(payment) AS sum "
            + "FROM invoice LEFT JOIN payment ON invoice.id=payment.invoice_id "
            + "WHERE (invoice.seller_id=? AND is_archival_for_seller=0) "
              + "OR (invoice.customer_id=?  AND is_archival_for_customer=0) "
            + "GROUP BY invoice.id";
    /**
     * Constant field specifies the query on search sum of all the payments
     *                    of the user invoices which contains pattern string.
     */
    private static final String SEARCH_PAYMENTS_SUM_FOR_USER =
            "SELECT SUM(payment) AS sum "
          + "FROM invoice LEFT JOIN payment ON invoice.id=payment.invoice_id "
          + "WHERE invoice.title LIKE CONCAT('%', ?, '%') " +
                "AND ((invoice.seller_id=? AND is_archival_for_seller=0) "
                    + "OR (invoice.customer_id=?  AND is_archival_for_customer=0)) "
          + "GROUP BY invoice.id "
          + "ORDER BY invoice.title";

    /**
     * Constant field specifies the query on search invoices with title
     *                                          which contains pattern string.
     */
    private static final String SEARCH_INVOICES =
            "SELECT invoice.id, invoice.title, SUM(price*amount) AS total_price, invoice.is_approved,  "
                    + "customer_id, customer.name AS customer_name, "
                    + "invoice.seller_id, seller.name AS seller_name "
          + "FROM invoice JOIN expense ON invoice.id=expense.invoice_id "
                       + "JOIN service ON service_id=service.id "
                       + "JOIN user seller ON invoice.seller_id=seller.id "
                       + "JOIN user customer ON customer_id=customer.id "
          + "WHERE invoice.title LIKE CONCAT('%', ?, '%') " +
                "AND ((invoice.seller_id=? AND is_archival_for_seller=0) "
                    + "OR (invoice.customer_id=?  AND is_archival_for_customer=0  AND is_approved=1)) "
          + "GROUP BY invoice.id "
          + "ORDER BY invoice.title";


    /**
     * Instantiates a new Invoice dao.
     *
     * @param connection the connection
     */
    public InvoiceDaoImpl(final ProxyConnection connection) {
        super(connection);
    }

    /**
     * Find all invoices of the seller.
     *
     * @param userId   the id of the user
     * @return the list
     */
    @Override
    public List<Invoice> findInvoices(final long userId) throws DaoException {
        List<Invoice> invoices = new LinkedList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(FIND_ALL);

            statement.setLong(1, userId);
            statement.setLong(2, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User seller = new User();
                seller.setId(resultSet.getLong("seller_id"));
                seller.setName(resultSet.getString("seller_name"));

                User customer = new User();
                customer.setId(resultSet.getLong("customer_id"));
                customer.setName(resultSet.getString("customer_name"));

                Invoice invoice = new Invoice();
                invoice.setId(resultSet.getLong("id"));
                invoice.setSeller(seller);
                invoice.setCustomer(customer);
                invoice.setTitle(resultSet.getString("title"));
                invoice.setTotalPrice(resultSet.getBigDecimal("total_price"));
                invoice.setApproved(resultSet.getInt("is_approved") == 1);
                invoices.add(invoice);
            }
        } catch (SQLException e) {
            throw new DaoException("The invoices of the user were not found."
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return invoices;
    }

    /**
     * Find invoice by id.
     *
     * @param id the id of invoice
     * @return the invoice
     */
    @Override
    public Invoice find(final long id) throws DaoException {
        PreparedStatement statement = null;
        Invoice invoice = new Invoice();
        invoice.setId(id);
        User seller = new User();
        User customer = new User();
        try {
            statement = connection.prepareStatement(FIND_INVOICE);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                fillInvoiceData(invoice,resultSet);
                fillUserData(seller, resultSet, "seller");
                fillUserData(customer, resultSet, "customer");
                invoice.setSeller(seller);
                invoice.setCustomer(customer);
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find the invoice. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return invoice;
    }

    /**
     * Find seller and customer id by invoice id.
     *
     * @param id the id of invoice
     * @return the array, where [0] = sellerId, [1] = customer_id
     * @throws DaoException the dao exception
     */
    @Override
    public long[] findSellerAndCustomerId(final long id) throws DaoException {
        PreparedStatement statement = null;
        long[] identifications = new long[2];
        try {
            statement = connection.prepareStatement(FIND_SELLER_AND_CUSTOMER);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                identifications[0] = resultSet.getLong("seller_id");
                identifications[1] = resultSet.getLong("customer_id");
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find the user. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return identifications;
    }

    /**
     * Add invoice.
     *
     * @param invoice the invoice
     * @return the id of invoice
     */
    @Override
    public long create(final Invoice invoice) throws DaoException {
        PreparedStatement statement = null;
        ResultSet resultId;
        long invoiceId = -1;
        try {
            statement = connection.prepareStatement(ADD_INVOICE,
                    Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, invoice.getTitle());
            statement.setString(2, invoice.getDescription());
            statement.setDate(3, invoice.getPaymentDeadline());
            statement.setLong(4, invoice.getCustomer().getId());
            statement.setLong(5, invoice.getSeller().getId());
            if (invoice.isApproved()) {
                statement.setInt(6, 1);
            } else {
                statement.setInt(6, 0);
            }
            statement.executeUpdate();
            resultId = statement.getGeneratedKeys();

            if (resultId.next()) {
                invoiceId = resultId.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("The invoice was not added. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return invoiceId;
    }

    /**
     * Archives invoice by id for seller or customer.
     *
     * @param id       the id of invoice
     * @param isSeller defines whether the user is the seller or the customer
     * @throws DaoException the dao exception
     */
    @Override
    public void remove(final long id, final boolean isSeller)
                                                throws DaoException {
        PreparedStatement statement = null;
        try {
            if (isSeller) {
                statement = connection.prepareStatement(ARCHIVE_FOR_SELLER);
            } else {
                statement = connection.prepareStatement(ARCHIVE_FOR_CUSTOMER);
            }
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("The invoice wasn't removed."
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
    }

    /**
     * Approve invoice.
     *
     * @param id       the id of invoice
     * @throws DaoException the dao exception
     */
    @Override
    public void approve(final long id) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(APPROVE_INVOICE);
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("The invoice wasn't approved."
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
    }

    /**
     * Find total price of the invoice.
     *
     * @param id the invoice id
     * @return the total price
     * @throws DaoException the dao exception
     */
    @Override
    public BigDecimal findInvoiceTotalPrice(final long id) throws DaoException {
        PreparedStatement statement = null;
        BigDecimal totalPrice = null;
        try {
            statement = connection.prepareStatement(FIND_INVOICE_PRICE);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                totalPrice = resultSet.getBigDecimal("total_price");
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find the total " +
                                "price of the invoice. " + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return totalPrice;
    }

    /**
     * Find sum of all the invoice payments.
     *
     * @param id the invoice id
     * @return the payments sum
     * @throws DaoException the dao exception
     */
    @Override
    public BigDecimal findSumOfAllInvoicePayments(final long id) throws DaoException {
        PreparedStatement statement = null;
        BigDecimal paymentsSum = null;
        try {
            statement = connection.prepareStatement(FIND_PAYMENTS_SUM);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                paymentsSum = resultSet.getBigDecimal("sum");
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find the payments sum. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return paymentsSum;
    }

    /**
     * Finds list of the sum of all the invoice payments for the user.
     *
     * @param userId the user id
     * @return the list of payments sum for the user
     * @throws DaoException the dao exception
     */
    @Override
    public List<BigDecimal> findInvoicesPaidPartsForUser(final long userId)
                                                                throws DaoException {
        PreparedStatement statement = null;
        List<BigDecimal> paymentsSum = new LinkedList<>();
        try {
            statement = connection.prepareStatement(FIND_PAYMENTS_SUM_FOR_USER);
            statement.setLong(1, userId);
            statement.setLong(2, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                paymentsSum.add(resultSet.getBigDecimal("sum"));
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find the payments sum "
                        + "for user, id = "+ userId + ". " + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return paymentsSum;
    }

    /**
     * Search list of the sum of all the payments of the invoice
     *          which contains pattern string in title for the user.
     *
     * @param searchQuery the search query
     * @param userId the user id
     * @return the list of payments sum for the user
     * @throws DaoException the dao exception
     */
    @Override
    public List<BigDecimal> searchOfInvoicesPaidPartsForUser(final String searchQuery,
                                                             final long userId)
                                                             throws DaoException {
        PreparedStatement statement = null;
        List<BigDecimal> paymentsSum = new LinkedList<>();
        try {
            statement = connection.prepareStatement(SEARCH_PAYMENTS_SUM_FOR_USER);
            statement.setString(1, searchQuery);
            statement.setLong(2, userId);
            statement.setLong(3, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                paymentsSum.add(resultSet.getBigDecimal("sum"));
            }
        } catch (SQLException e) {
            throw new DaoException(String.format("It wasn't succeeded to find the payments sum "
                        + "for user, id = % and invoices with title containing search query \"%s\" "
                        + e.getMessage(), userId, searchQuery), e);
        } finally {
            closeStatement(statement);
        }
        return paymentsSum;
    }

    /**
     * Search of the invoices which contains pattern string in title.
     *
     * @param searchQuery the search query
     * @param userId   the id of the user
     * @return the list
     */
    @Override
    public List<Invoice> searchOfInvoices(final String searchQuery,
                                          final long userId) throws DaoException {
        List<Invoice> invoices = new LinkedList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SEARCH_INVOICES);

            statement.setString(1, searchQuery);
            statement.setLong(2, userId);
            statement.setLong(3, userId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                User customer = new User();
                customer.setId(resultSet.getLong("customer_id"));
                customer.setName(resultSet.getString("customer_name"));

                User seller = new User();
                seller.setId(resultSet.getLong("seller_id"));
                seller.setName(resultSet.getString("seller_name"));

                Invoice invoice = new Invoice();
                invoice.setId(resultSet.getLong("id"));
                invoice.setSeller(seller);
                invoice.setCustomer(customer);
                invoice.setTitle(resultSet.getString("title"));
                invoice.setTotalPrice(resultSet.getBigDecimal("total_price"));
                invoice.setApproved(resultSet.getInt("is_approved") == 1);
                invoices.add(invoice);
            }
        } catch (SQLException e) {
            throw new DaoException(String.format("It was not succeeded to "
                    + "find the invoices with title containing search query \"%s\" "
                    + e.getMessage(), searchQuery), e);
        } finally {
            closeStatement(statement);
        }
        return invoices;
    }

    /**
     * Fills the seller of customer data.
     *
     * @param user the user
     * @param resultSet the result set
     * @param role the user is customer or seller
     * @throws SQLException the sql exception
     */
    private void fillUserData(final User user,
                              final ResultSet resultSet, final String role) throws SQLException {
        user.setId(resultSet.getLong(role + "_id"));
        user.setEmail(resultSet.getString(role + "_email"));
        user.setName(resultSet.getString(role + "_name"));
        user.setPhone(resultSet.getString(role + "_phone"));
    }

    /**
     * Fills the invoice data.
     *
     * @param invoice the invoice
     * @param resultSet the result set
     * @throws SQLException the sql exception
     */
    private void fillInvoiceData(final Invoice invoice,
                                 final ResultSet resultSet) throws SQLException {
        invoice.setTitle(resultSet.getString("title"));
        invoice.setDescription(resultSet.getString("description"));
        invoice.setCreatedDate(resultSet.getDate("created_date"));
        invoice.setPaymentDeadline(resultSet.getDate("payment_deadline"));
        invoice.setArchivalForSeller(resultSet.getInt(
                                    "is_archival_for_seller") == 1);
        invoice.setArchivalForCustomer(resultSet.getInt(
                                    "is_archival_for_customer") == 1);
        invoice.setApproved(resultSet.getInt( "is_approved") == 1);
    }
}
