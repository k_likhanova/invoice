package by.training.invoice.dao.impl;

import by.training.invoice.connection.ProxyConnection;
import by.training.invoice.dao.AbstractDao;
import by.training.invoice.dao.UserDao;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.DaoException;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

/**
 * The type User dao.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
@NoArgsConstructor
public class UserDaoImpl extends AbstractDao implements UserDao {
    /**
     * Constant field specifies the query on addition the user.
     */
    private static final String ADD_USER =
            "INSERT INTO user (email,password,name,phone) " +
                    "values(?,MD5(?),?,?)";
    /**
     * Constant field specifies the query on find the user by email.
     */
    private static final String FIND_BY_EMAIL = "SELECT id FROM user WHERE email=?";
    /**
     * Constant field specifies the query on find the user by email and password.
     */
    private static final String FIND_BY_EMAIL_AND_PASSWORD =
                             "SELECT * FROM user WHERE email=? AND password=MD5(?)";
    /**
     * Constant field specifies the query on find the user.
     */
    private static final String FIND_USER =
                             "SELECT * FROM user WHERE id=?";
    /**
     * Constant field specifies the query on find the user current balance.
     */
    private static final String FIND_USER_BALANCE =
                              "SELECT current_balance FROM user WHERE id=?";
    /**
     * Constant field specifies the query on update the user data.
     */
    private static final String UPDATE_USER_DATA =
            "UPDATE user SET email=?,name=?,phone=? WHERE id=?";
    /**
     * Constant field specifies the query on update the current balance.
     */
    private static final String UPDATE_BALANCE =
                                    "UPDATE user SET current_balance=? WHERE id=?";
    /**
     * Constant field specifies the query on find invoices summary of the customer.
     */
    private static final String FIND_INVOICES_SUMMARY =
            "SELECT SUM(exp.total_price) AS invoices_summary " +
            "FROM invoice JOIN " +
                    "(SELECT price*amount AS total_price, expense.invoice_id " +
                    "FROM expense JOIN service ON service_id=service.id) AS exp " +
            "ON invoice.id=exp.invoice_id " +
            "WHERE customer_id=?";
    /**
     * Constant field specifies the query on find payments summary of the customer.
     */
    private static final String FIND_PAYMENTS_SUMMARY =
            "SELECT SUM(payment) AS payments_summary " +
            "FROM invoice JOIN payment ON invoice.id=payment.invoice_id " +
            "WHERE customer_id=?";
    /**
     * Constant field specifies the query on find the all user.
     */
    private static final String FIND_ALL =
            "SELECT id, email, name FROM user ORDER BY name";

    /**
     * Constant field specifies the query on search users with name or email
     *                                          which contains pattern string.
     */
    private static final String SEARCH_USERS =
            "SELECT id, email, name FROM user "
          + "WHERE user.name LIKE CONCAT('%', ?, '%') OR email LIKE CONCAT('%', ?, '%') "
          + "ORDER BY name;";

    /**
     * Instantiates a new User dao.
     *
     * @param connection the connection
     */
    public UserDaoImpl(final ProxyConnection connection) {
        super(connection);
    }

    /**
     * Find all users.
     *
     * @return the list of users
     */
    @Override
    public List<User> findAll() throws DaoException {
        Statement statement = null;
        List<User> users = new LinkedList<>();
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getLong("id"));
                user.setName(resultSet.getString("name"));
                user.setEmail(resultSet.getString("email"));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find the users. "
                                                        + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return users;
    }

    /**
     * Find user by id.
     *
     * @param id the id of user
     * @return the user
     */
    @Override
    public User find(final long id) throws DaoException {
        PreparedStatement statement = null;
        User user = new User();
        try {
            statement = connection.prepareStatement(FIND_USER);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                fillUserData(user, resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find the user. "
                                                        + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return user;
    }

    /**
     * Add user.
     *
     * @param user the new user
     * @return the id of user
     */
    @Override
    public long create(final User user) throws DaoException {
        PreparedStatement userStatement = null;
        ResultSet resultId;
        long userId;
        try {
            userStatement = connection.prepareStatement(ADD_USER,
                                        Statement.RETURN_GENERATED_KEYS);
            userStatement.setString(1, user.getEmail());
            userStatement.setString(2, user.getPassword());
            userStatement.setString(3, user.getName());
            userStatement.setString(4, user.getPhone());
            userStatement.executeUpdate();
            resultId = userStatement.getGeneratedKeys();

            resultId.next();
            userId = resultId.getLong(1);
        } catch (SQLException e) {
            throw new DaoException("The user was not added. "
                                        + e.getMessage(), e);
        } finally {
            closeStatement(userStatement);
        }
        return userId;
    }

    /**
     * Update entity.
     *
     * @param user the new user data
     */
    @Override
    public void update(final User user) throws DaoException {
        PreparedStatement userStatement = null;
        try {
            userStatement = connection.prepareStatement(UPDATE_USER_DATA);
            userStatement.setString(1, user.getEmail());
            userStatement.setString(2, user.getName());
            userStatement.setString(3, user.getPhone());
            userStatement.setLong(4, user.getId());
            userStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("The user data was not updated. "
                                                + e.getMessage(), e);
        } finally {
            closeStatement(userStatement);
        }
    }

    /**
     * Finds user id by email.
     *
     * @param email the email
     * @return the user id
     * @throws DaoException the dao exception
     */
    @Override
    public long findUserIdByEmail(final String email) throws DaoException {
        PreparedStatement statement = null;
        long userId = -1;
        try {
            statement = connection.prepareStatement(FIND_BY_EMAIL);
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                userId = resultSet.getLong("id");
            }
        } catch (SQLException e) {
            throw new DaoException("It was not succeeded to "
                            + "find the user id by email. ", e);
        } finally {
            closeStatement(statement);
        }
        return userId;
    }

    /**
     * Find user by email user.
     *
     * @param email    the email
     * @param password the password
     * @return the user
     * @throws DaoException the dao exception
     */
    @Override
    public User findUserByEmail(final String email,
                                final String password) throws DaoException {
        PreparedStatement statement = null;
        User user = null;
        try {
            statement = connection.prepareStatement(FIND_BY_EMAIL_AND_PASSWORD);
            statement.setString(1, email);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                fillUserData(user, resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException("It was not succeeded to find the user. "
                                                        + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return user;
    }

    /**
     * Finds current balance of the user.
     *
     * @param userId the id of user
     * @throws DaoException the dao exception
     */
    @Override
    public BigDecimal findCurrentBalance(final long userId) throws DaoException {
        PreparedStatement statement = null;
        BigDecimal currentBalance = BigDecimal.ZERO;
        try {
            statement = connection.prepareStatement(FIND_USER_BALANCE);
            statement.setLong(1, userId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                currentBalance = resultSet.getBigDecimal("current_balance");
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to recharge balance od user. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return currentBalance;
    }

    /**
     * Recharges current balance on the moneyAmount of the user with id=userId.
     *
     * @param currentBalance the current balance
     * @param moneyAmount the amount of money for recharging of the balance
     * @param userId the id of user
     * @return the new current balance
     * @throws DaoException the dao exception
     */
    @Override
    public BigDecimal rechargeBalance(final BigDecimal currentBalance,
                                final BigDecimal moneyAmount,
                                final long userId) throws DaoException {
        BigDecimal newCurrentBalance = currentBalance.add(moneyAmount);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UPDATE_BALANCE);
            statement.setBigDecimal(1, newCurrentBalance);
            statement.setLong(2, userId);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to recharge balance of user. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return newCurrentBalance;
    }

    /**
     * Pay invoice.
     *
     * @param currentBalance the current balance
     * @param payment the amount of money for payment
     * @param userId the id of user
     * @throws DaoException the dao exception
     */
    @Override
    public void pay(final BigDecimal currentBalance, final BigDecimal payment,
                                            final long userId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UPDATE_BALANCE);
            statement.setBigDecimal(1, currentBalance.subtract(payment));
            statement.setLong(2, userId);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to pay. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
    }

    /**
     * Find invoices summary of the customer.
     *
     * @param customerId the customer id of the invoices
     * @return the invoices summary
     * @throws DaoException the dao exception
     */
    @Override
    public BigDecimal findInvoicesSummary(final long customerId) throws DaoException {
        PreparedStatement statement = null;
        BigDecimal invoicesSummary = null;
        try {
            statement = connection.prepareStatement(FIND_INVOICES_SUMMARY);
            statement.setLong(1, customerId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                invoicesSummary = resultSet.getBigDecimal("invoices_summary");
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find "
                                    + "the invoices_summary. " + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return invoicesSummary;
    }

    /**
     * Find payments summary of the customer.
     *
     * @param customerId the customer id of the invoices
     * @return the payments summary
     * @throws DaoException the dao exception
     */
    @Override
    public BigDecimal findPaymentsSummary(final long customerId) throws DaoException {
        PreparedStatement statement = null;
        BigDecimal paymentsSummary = null;
        try {
            statement = connection.prepareStatement(FIND_PAYMENTS_SUMMARY);
            statement.setLong(1, customerId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                paymentsSummary = resultSet.getBigDecimal("payments_summary");
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find "
                                    + "the payments summary. " + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return paymentsSummary;
    }

    /**
     * Search of the users which contains pattern string in name or email.
     *
     * @param searchQuery the search query
     * @return the users
     * @throws DaoException the dao exception
     */
    @Override
    public List<User> searchOfUsers(final String searchQuery) throws DaoException {
        PreparedStatement statement = null;
        List<User> users = new LinkedList<>();
        try {
            statement = connection.prepareStatement(SEARCH_USERS);
            statement.setString(1, searchQuery);
            statement.setString(2, searchQuery);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getLong("id"));
                user.setEmail(resultSet.getString("email"));
                user.setName(resultSet.getString("name"));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DaoException(String.format("It was not succeeded to "
                    + "find the users with name or email containing search query \"%s\" "
                    + e.getMessage(), searchQuery), e);
        } finally {
            closeStatement(statement);
        }
        return users;
    }

    /**
     * Fills the user data.
     *
     * @param user the user
     * @param resultSet the result set
     * @throws SQLException the sql exception
     */
    private void fillUserData(final User user,
                              final ResultSet resultSet) throws SQLException {
        user.setId(resultSet.getLong("id"));
        user.setEmail(resultSet.getString("email"));
        user.setName(resultSet.getString("name"));
        user.setPhone(resultSet.getString("phone"));
        user.setCurrentBalance(resultSet.getBigDecimal("current_balance"));
    }


}
