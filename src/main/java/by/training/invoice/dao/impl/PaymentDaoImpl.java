package by.training.invoice.dao.impl;

import by.training.invoice.connection.ProxyConnection;
import by.training.invoice.dao.AbstractDao;
import by.training.invoice.dao.PaymentDao;
import by.training.invoice.entity.impl.Payment;
import by.training.invoice.exception.DaoException;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.math.BigDecimal;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * The type Payment dao.
 * Created on 12.01.2018.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
@NoArgsConstructor
public class PaymentDaoImpl extends AbstractDao implements PaymentDao {

    /**
     * Constant field specifies the query on find all payments in invoice.
     */
    private static final String FIND_PAYMENTS =
            "SELECT id, payment, date FROM payment WHERE invoice_id=?";
    /**
     * Constant field specifies the query on add payment.
     */
    private static final String ADD_PAYMENT =
            "INSERT INTO payment (payment,invoice_id) values(?,?)";

    /**
     * Instantiates a new Payment dao.
     *
     * @param connection the connection
     */
    public PaymentDaoImpl(final ProxyConnection connection) {
        super(connection);
    }

    /**
     * Add payment.
     *
     * @param payment the payment
     * @param invoiceId to what invoice the payment belongs
     * @return the id of payment
     * @throws DaoException the dao exception
     */
    @Override
    public long create(final BigDecimal payment,
                       final long invoiceId) throws DaoException {
        PreparedStatement statement = null;
        long paymentId;
        try {
            statement = connection.prepareStatement(ADD_PAYMENT,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setBigDecimal(1, payment);
            statement.setLong(2, invoiceId);
            statement.executeUpdate();

            ResultSet resultId = statement.getGeneratedKeys();
            resultId.next();
            paymentId = resultId.getLong(1);
        } catch (SQLException e) {
            throw new DaoException("The payment wasn't added."
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return paymentId;
    }

    /**
     * Find all payments in invoice.
     *
     * @param invoiceId the invoice id
     * @return the list
     * @throws DaoException the dao exception
     */
    @Override
    public List<Payment> findAll(final long invoiceId) throws DaoException {
        PreparedStatement statement = null;
        List<Payment> payments = new LinkedList<>();
        try {
            statement = connection.prepareStatement(FIND_PAYMENTS);
            statement.setLong(1, invoiceId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Payment payment = new Payment();
                payment.setPayment(resultSet.getBigDecimal("payment"));

                payment.setDate(resultSet.getDate("date"));
                payment.setTime(resultSet.getTime("date"));
                payment.setId(resultSet.getLong("id"));
                payments.add(payment);
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find the expenses. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return payments;
    }
}
