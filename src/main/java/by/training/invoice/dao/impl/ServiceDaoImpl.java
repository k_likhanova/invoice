package by.training.invoice.dao.impl;

import by.training.invoice.connection.ProxyConnection;
import by.training.invoice.dao.AbstractDao;
import by.training.invoice.dao.ServiceDao;
import by.training.invoice.entity.impl.Service;
import by.training.invoice.exception.DaoException;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

/**
 * The type Service dao.
 * Created on 04.01.2018.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
@NoArgsConstructor
public class ServiceDaoImpl extends AbstractDao implements ServiceDao {
    /**
     * Constant field specifies the query on find the service.
     */
    private static final String FIND_SERVICE =
            "SELECT * FROM service WHERE id=?";
    /**
     * Constant field specifies the query on find all services.
     */
    private static final String FIND_ALL =
            "SELECT * FROM service ORDER BY title";
    /**
     * Constant field specifies the query on add the service.
     */
    private static final String ADD_SERVICE =
            "INSERT INTO service (title,description,price,"
                    + "seller_id,is_archival) values(?,?,?,?,?)";
    /**
     * Constant field specifies the query on archive the service.
     */
    private static final String ARCHIVE =
            "UPDATE service SET is_archival=1 WHERE id=?";

    private static final String FIND_USER_SERVICES =
            "SELECT * FROM service where seller_id=? AND is_archival=0";

    /**
     * Instantiates a new Service dao.
     *
     * @param connection the connection
     */
    public ServiceDaoImpl(final ProxyConnection connection) {
        super(connection);
    }

    /**
     * Find all services.
     *
     * @return the list
     */
    @Override
    public List<Service> findAll() throws DaoException {
        Statement statement = null;
        List<Service> services = new LinkedList<>();
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                Service service = new Service();
                fillServiceData(service, resultSet);
                services.add(service);
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find services. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return services;
    }

    /**
     * Find service by id.
     *
     * @param id the id of service
     * @return the service
     */
    @Override
    public Service find(final long id) throws DaoException {
        PreparedStatement statement = null;
        Service service = new Service();
        try {
            statement = connection.prepareStatement(FIND_SERVICE);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                fillServiceData(service, resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find the user. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return service;
    }

    /**
     * Archives service by id.
     *
     * @param id the id of service
     */
    @Override
    public void remove(final long id) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(ARCHIVE);

            statement.setLong(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("The service wasn't archived."
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
    }

    /**
     * Add user.
     *
     * @param service the new service
     */
    @Override
    public long create(final Service service) throws DaoException {
        PreparedStatement statement = null;
        long serviceId = -1;
        try {
            statement = connection.prepareStatement(ADD_SERVICE,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, service.getTitle());
            statement.setString(2, service.getDescription());
            statement.setBigDecimal(3, service.getPrice());
            statement.setLong(4, service.getSellerId());
            statement.setInt(5, 0);
            statement.executeUpdate();

            ResultSet resultId = statement.getGeneratedKeys();
            if (resultId.next()) {
                serviceId = resultId.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("The service wasn't added."
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return serviceId;
    }

    /**
     * Find all services of the user by user id.
     *
     * @param userId the user id
     * @return the services list
     * @throws DaoException the dao exception
     */
    @Override
    public List<Service> findUserServices(final long userId) throws DaoException {
        PreparedStatement statement = null;
        List<Service> services = new LinkedList<>();
        try {
            statement = connection.prepareStatement(FIND_USER_SERVICES);
            statement.setLong(1, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Service service = new Service();
                fillServiceData(service, resultSet);
                services.add(service);
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find services of user. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return services;
    }


    /**
     * Fills the service data.
     *
     * @param service   the object of service
     * @param resultSet the resultSet
     * @throws SQLException the sql exception
     */
    static void fillServiceData(final Service service,
                                 final ResultSet resultSet) throws SQLException {
        service.setId(resultSet.getLong("id"));
        service.setTitle(resultSet.getString("title"));
        service.setDescription(resultSet.getString("description"));
        service.setPrice(resultSet.getBigDecimal("price"));
        service.setSellerId(resultSet.getLong("seller_id"));
        service.setArchival(resultSet.getInt("is_archival") == 1);
    }

}
