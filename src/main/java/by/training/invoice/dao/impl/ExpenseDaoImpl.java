package by.training.invoice.dao.impl;

import by.training.invoice.connection.ProxyConnection;
import by.training.invoice.dao.AbstractDao;
import by.training.invoice.dao.ExpenseDao;
import by.training.invoice.entity.impl.Expense;
import by.training.invoice.entity.impl.Service;
import by.training.invoice.exception.DaoException;
import lombok.extern.log4j.Log4j2;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

/**
 * The type Expense dao.
 * Created on 05.01.2018.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public class ExpenseDaoImpl extends AbstractDao implements ExpenseDao {
    /**
     * Constant field specifies the query on find all expenses in invoice.
     */
    private static final String FIND_EXPENSES =
            "SELECT service.title AS service_title, service.description AS service_description, "
                + "expense.amount, (price*amount) AS expense_price "
          + "FROM expense JOIN service ON service_id=service.id "
          + "WHERE invoice_id=?";
    /**
     * Constant field specifies the query on add service.
     */
    private static final String ADD_EXPENSE =
            "INSERT INTO expense (invoice_id,service_id,amount) values(?,?,?)";

    /**
     * Instantiates a new Expense dao.
     *
     * @param connection the connection
     */
    public ExpenseDaoImpl(final ProxyConnection connection) {
        super(connection);
    }

    /**
     * Find all services in invoice.
     *
     * @param invoiceId the invoice id
     * @return the list
     * @throws DaoException the dao exception
     */
    @Override
    public List<Expense> findAll(final long invoiceId) throws DaoException {
        PreparedStatement statement = null;
        List<Expense> expenses = new LinkedList<>();
        try {
            statement = connection.prepareStatement(FIND_EXPENSES);
            statement.setLong(1, invoiceId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Service service = new Service();
                service.setTitle(resultSet.getString("service_title"));
                service.setDescription(resultSet.getString("service_description"));

                Expense expense = new Expense();
                expense.setService(service);
                expense.setServiceAmount(resultSet.getInt("amount"));
                expense.setTotalPrice(resultSet.getBigDecimal("expense_price"));

                expenses.add(expense);
            }
        } catch (SQLException e) {
            throw new DaoException("It wasn't succeeded to find the expenses. "
                    + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return expenses;
    }

    /**
     * Add expense.
     *
     * @param invoiceId     to what invoice the service belongs.
     * @param serviceId     the service id
     * @param serviceAmount the amount of service
     * @return the id of expense
     * @throws DaoException the dao exception
     */
    @Override
    public long create(final long invoiceId, final long serviceId,
                       final int serviceAmount) throws DaoException {
        PreparedStatement statement = null;
        long expenseId;
        try {
            statement = connection.prepareStatement(ADD_EXPENSE,
                                        Statement.RETURN_GENERATED_KEYS);

            statement.setLong(1, invoiceId);
            statement.setLong(2, serviceId);
            statement.setInt(3, serviceAmount);
            statement.executeUpdate();

            ResultSet resultId = statement.getGeneratedKeys();
            resultId.next();
            expenseId = resultId.getLong(1);
        } catch (SQLException e) {
            throw new DaoException("The expense wasn't added."
                                            + e.getMessage(), e);
        } finally {
            closeStatement(statement);
        }
        return expenseId;
    }
}
