package by.training.invoice.dao;

import by.training.invoice.connection.ConnectionPool;
import by.training.invoice.connection.ProxyConnection;
import by.training.invoice.exception.DaoException;
import by.training.invoice.exception.DbConnectionException;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * The type Abstract dao.
 * Created on 22.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
@Log4j2
@NoArgsConstructor
public abstract class AbstractDao {
    /**
     * The Connection with DB.
     */
    protected ProxyConnection connection;

    /**
     * Instantiates a new Abstract dao.
     *
     * @param newConnection the connection
     */
    public AbstractDao(final ProxyConnection newConnection) {
        this.connection = newConnection;
    }

    /**
     * Sets connection.
     *
     * @throws DaoException the dao exception
     */
    public void setConnection() throws DaoException {
        try {
            this.connection = ConnectionPool.getInstance().takeConnection();
        } catch (DbConnectionException e) {
            throw new DaoException( e);
        }
    }

    /**
     * Close connection.
     */
    public void closeConnection() {
        ConnectionPool.getInstance().freeConnection(connection);
    }

    /**
     * Close statement.
     *
     * @param statement the statement
     */
    protected void closeStatement(final Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            log.log(Level.ERROR, "It is impossible to closeConnection statement.", e);
        }
    }

}
