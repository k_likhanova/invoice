/**
 * Provides the classes necessary to realization
 *                  of interaction with the database.
 * Pattern Dao is used.
 */
package by.training.invoice.dao;
