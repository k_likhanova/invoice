package by.training.invoice.dao;

import by.training.invoice.entity.Entity;
import by.training.invoice.exception.DaoException;

import java.util.List;

/**
 * The interface Dao.
 * Created on 22.12.2018.
 *
 * @param <T> the entity
 * @author Kseniya Likhanova
 * @version 1.0
 */
public interface Dao<T extends Entity> {
    /**
     * Find all entities.
     *
     * @return the list
     * @throws DaoException the dao exception
     */
    List<T> findAll() throws DaoException;

    /**
     * Find entity by id.
     *
     * @param id the id of entity
     * @return the entity
     * @throws DaoException the dao exception
     */
    T find(long id) throws DaoException;

    /**
     * Add entity.
     *
     * @param entity the entity
     * @return the id of entity
     * @throws DaoException the dao exception
     */
    long create(T entity) throws DaoException;
}
