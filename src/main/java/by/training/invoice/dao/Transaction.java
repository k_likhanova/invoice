package by.training.invoice.dao;

import by.training.invoice.connection.ConnectionPool;
import by.training.invoice.connection.ProxyConnection;
import by.training.invoice.exception.DaoException;
import lombok.extern.log4j.Log4j2;

import java.sql.SQLException;

/**
 * The type Transaction.
 * Created on 05.01.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
@Log4j2
public class Transaction {
    /**
     * The Connection with DB.
     */
    protected ProxyConnection connection;

    /**
     * Instantiates a new Transaction.
     *
     * @throws DaoException the dao exception
     */
    public Transaction() throws DaoException {
        try {
            this.connection = ConnectionPool.getInstance().takeConnection();
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    /**
     * Gets connection.
     *
     * @return the connection
     */
    public ProxyConnection getConnection() {
        return connection;
    }

    /**
     * Commit.
     *
     * @throws DaoException the dao exception
     */
    public void commit() throws DaoException {
        try {
            connection.commit();
        } catch(SQLException e) {
            throw new DaoException("It is impossible to commit transaction"
                                                        + e.getMessage(), e);
        }
    }

    /**
     * Rollback.
     */
    public void rollback() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            log.error("It is impossible to rollback"
                                    + e.getMessage(), e);
        }
    }

    /**
     * Close connection.
     */
    public void closeConnection() {
        ConnectionPool.getInstance().freeConnection(connection);
    }
}
