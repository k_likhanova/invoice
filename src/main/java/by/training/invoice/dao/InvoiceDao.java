package by.training.invoice.dao;

import by.training.invoice.entity.impl.Invoice;
import by.training.invoice.exception.DaoException;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public interface InvoiceDao extends Dao<Invoice> {
    /**
     * Find all invoices of the seller.
     *
     * @param userId   the id of the user
     * @return the list
     */
    List<Invoice> findInvoices(final long userId) throws DaoException;

    /**
     * Find seller and customer id by invoice id.
     *
     * @param id the id of invoice
     * @return the array, where [0] = sellerId, [1] = customer_id
     * @throws DaoException the dao exception
     */
    long[] findSellerAndCustomerId(final long id) throws DaoException;

    /**
     * Archives invoice by id for seller or customer.
     *
     * @param id       the id of invoice
     * @param isSeller defines whether the user is the seller or the customer
     * @throws DaoException the dao exception
     */
    void remove(final long id, final boolean isSeller)
            throws DaoException;

    /**
     * Approve invoice.
     *
     * @param id       the id of invoice
     * @throws DaoException the dao exception
     */
    void approve(final long id) throws DaoException;

    /**
     * Find total price of the invoice.
     *
     * @param id the invoice id
     * @return the total price
     * @throws DaoException the dao exception
     */
    BigDecimal findInvoiceTotalPrice(final long id) throws DaoException;

    /**
     * Find sum of all the invoice payments.
     *
     * @param id the invoice id
     * @return the payments sum
     * @throws DaoException the dao exception
     */
    BigDecimal findSumOfAllInvoicePayments(final long id) throws DaoException;

    /**
     * Finds list of the sum of all the invoice payments for the user.
     *
     * @param userId the user id
     * @return the list of payments sum for the user
     * @throws DaoException the dao exception
     */
    List<BigDecimal> findInvoicesPaidPartsForUser(final long userId)
            throws DaoException;

    /**
     * Search list of the sum of all the payments of the invoice
     *          which contains pattern string in title for the user.
     *
     * @param searchQuery the search query
     * @param userId the user id
     * @return the list of payments sum for the user
     * @throws DaoException the dao exception
     */
    List<BigDecimal> searchOfInvoicesPaidPartsForUser(final String searchQuery,
                                                             final long userId)
            throws DaoException;

    /**
     * Search of the invoices which contains pattern string in title.
     *
     * @param searchQuery the search query
     * @param userId   the id of the user
     * @return the list
     */
    List<Invoice> searchOfInvoices(final String searchQuery,
                                          final long userId) throws DaoException;

    /**
     * Find all entities.
     *
     * @return the list
     */
    @Override
    default List<Invoice> findAll() {
        return new LinkedList<>();
    }


}
