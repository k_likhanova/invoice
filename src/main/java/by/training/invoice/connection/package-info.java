/**
 * Provides the classes necessary to connection with the database.
 * Pattern Proxy is used.
 */
package by.training.invoice.connection;

