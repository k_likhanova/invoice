package by.training.invoice.connection;

import by.training.invoice.exception.DbConnectionException;
import lombok.extern.log4j.Log4j2;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Connection pool.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 */
@Log4j2
public final class ConnectionPool {
    /**
     * Field instance specifies the single object of ConnectionPool.
     */
    private static ConnectionPool instance;
    /**
     * Field specifies whether the connection pool is created.
     */
    private static AtomicBoolean isInitialized = new AtomicBoolean(false);
    /**
     * The constant specifies the lock.
     */
    private static ReentrantLock lock = new ReentrantLock();

    /**
     * Field specifies free connections.
     */
    private BlockingQueue<ProxyConnection> freeConnections;
    /**
     * Field specifies used connections.
     */
    private Set<ProxyConnection> usedConnections;

    /**
     * Default constructor of ConnectionPool.
     * Uses Singleton pattern therefor private.
     */
    private ConnectionPool() {
        this.freeConnections = new LinkedBlockingQueue<>();
        this.usedConnections = new ConcurrentSkipListSet<>();
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static ConnectionPool getInstance() {
        if (!isInitialized.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    isInitialized.compareAndSet(false, true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Initializes the poolConnection.
     *
     * @throws DbConnectionException the db connection exception
     */
    public void initialize() throws DbConnectionException {
        try {
            Class.forName(DbConnector.DRIVER);
            for (int counter = 0; counter < DbConnector.START_POOL_SIZE; counter++) {
                freeConnections.put(createConnection());
            }
        } catch (ClassNotFoundException | SQLException | InterruptedException e) {
            log.fatal("It is impossible to initialize connection pool", e);
            throw new DbConnectionException(e);
        }
    }

    /**
     * Creates connection.
     *
     * @return the new ProxyConnection
     * @throws SQLException the sql exception
     */
    private ProxyConnection createConnection() throws SQLException {
        return new ProxyConnection(DbConnector.getConnection());
    }

    /**
     * Closes connection.
     *
     * @param connection the proxy connection which needs to be closed
     */
    private void closeConnection(final ProxyConnection connection) {
        try {
            connection.getConnection().close();
        } catch (SQLException e) {
            log.info(e);
        }
    }

    /**
     * Takes free proxy connection.
     *
     * @return the proxy connection
     * @throws DbConnectionException the db connection exception
     */
    public ProxyConnection takeConnection() throws DbConnectionException {
        ProxyConnection connection = null;
        while (connection == null) {
            try {
                if (!freeConnections.isEmpty()) {
                    connection = freeConnections.take();
                    if (!connection.isValid(DbConnector.CONNECTION_TIMEOUT)) {
                        closeConnection(connection);
                        connection = null;
                    }
                } else if (usedConnections.size() < DbConnector.POOL_SIZE) {
                    connection = createConnection();
                } else {
                    log.error("The limit of number of database connections "
                                                                + "is exceeded");
                }
            } catch (SQLException | InterruptedException e) {
                log.error("It is impossible to connect to a database", e);
                throw new DbConnectionException(e);
            }
        }
        usedConnections.add(connection);
        return connection;
    }

    /**
     * Frees connection.
     *
     * @param connection the connection
     */
    public void freeConnection(final ProxyConnection connection) {
        try {
            if (connection.isValid(DbConnector.CONNECTION_TIMEOUT)) {
                usedConnections.remove(connection);
                freeConnections.put(connection);
            }
        } catch (SQLException | InterruptedException e) {
            log.error("It is impossible to return database connection into pool. ", e);
            closeConnection(connection);
        }
    }

    /**
     * Destroy.
     */
    public void destroy() {
        for (ProxyConnection connection : usedConnections) {
            freeConnection(connection);
        }
        try {
            while (!freeConnections.isEmpty()) {
                closeConnection(freeConnections.take());
            }
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
        try {
            Enumeration<Driver> drivers = DriverManager.getDrivers();
            while (drivers.hasMoreElements()) {
                Driver driver = drivers.nextElement();
                DriverManager.deregisterDriver(driver);
            }
        } catch (SQLException e) {
            log.warn(e);
        }
    }
}
