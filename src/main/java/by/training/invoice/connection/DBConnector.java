package by.training.invoice.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Database connector.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 */
final class DbConnector {
    /**
     * The object of resource bundle.
     */
    private static ResourceBundle resource = ResourceBundle.getBundle("database");
    /**
     * The Driver.
     */
    static final String DRIVER = resource.getString("db.driver");
    /**
     * The Start pool size.
     */
    static final int START_POOL_SIZE =
                        Integer.parseInt(resource.getString("db.startPoolSize"));
    /**
     * The Pool size.
     */
    static final int POOL_SIZE =
                        Integer.parseInt(resource.getString("db.poolSize"));
    /**
     * The Connection timeout.
     */
    static final int CONNECTION_TIMEOUT =
                        Integer.parseInt(resource.getString("db.connectionTimeout"));

    /**
     * Private constructor, because all method is static.
     */
    private DbConnector() { }

    /**
     * Gets connection.
     *
     * @return the connection
     * @throws SQLException the sql exception
     */
    static Connection getConnection() throws SQLException {
        String url = resource.getString("db.url");
        String user = resource.getString("db.user");
        String pass = resource.getString("db.password");
        return DriverManager.getConnection(url, user, pass);
    }
}
