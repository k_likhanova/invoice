package by.training.invoice.exception;

/**
 * The type Logic exception.
 * Created on 29.12.2018.
 *
 * @author Kseniya Likhanova
 */
public class LogicException extends Exception {
    /**
     * Instantiates a new Logic exception.
     */
    public LogicException() {
    }

    /**
     * Instantiates a new Logic exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public LogicException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Logic exception.
     *
     * @param message the message
     */
    public LogicException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new Logic exception.
     *
     * @param cause the cause
     */
    public LogicException(final Throwable cause) {
        super(cause);
    }
}
