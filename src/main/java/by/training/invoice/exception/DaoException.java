package by.training.invoice.exception;

import java.sql.SQLException;

/**
 * The type Dao exception.
 * Created on 29.12.2018.
 *
 * @author Kseniya Likhanova
 */
public class DaoException extends SQLException {
    /**
     * Instantiates a new Dao exception.
     */
    public DaoException() {
    }

    /**
     * Instantiates a new Dao exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public DaoException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Dao exception.
     *
     * @param message the message
     */
    public DaoException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new Dao exception.
     *
     * @param cause the cause
     */
    public DaoException(final Throwable cause) {
        super(cause);
    }
}
