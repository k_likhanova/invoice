package by.training.invoice.exception;

import java.sql.SQLException;

/**
 * The type database connection exception.
 * Created on 23.12.2018.
 *
 * @author Kseniya Likhanova
 */
public class DbConnectionException extends SQLException {
    /**
     * Instantiates a new Db connection exception.
     */
    public DbConnectionException() {
    }

    /**
     * Instantiates a new Db connection exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public DbConnectionException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Db connection exception.
     *
     * @param message the message
     */
    public DbConnectionException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new Db connection exception.
     *
     * @param cause the cause
     */
    public DbConnectionException(final Throwable cause) {
        super(cause);
    }
}
