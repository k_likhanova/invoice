package by.training.invoice.logic;

import by.training.invoice.dao.Transaction;
import by.training.invoice.dao.impl.ServiceDaoImpl;
import by.training.invoice.dao.impl.UserDaoImpl;
import by.training.invoice.entity.impl.Service;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.DaoException;
import by.training.invoice.exception.LogicException;
import by.training.invoice.util.DecimalParser;
import by.training.invoice.validator.UserValidator;
import by.training.invoice.validator.UtilValidator;
import lombok.extern.log4j.Log4j2;

import java.math.BigDecimal;
import java.util.List;

/**
 * The type User logic.
 * Created on 24.12.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
@Log4j2
public class UserLogic {
    /**
     * Registration.
     *
     * @param email     the email
     * @param password  the password
     * @param name      the name
     * @param phone     the phone
     * @return the boolean
     * @throws LogicException the logic exception
     */
    public int registration(final String email, final String password,
                                final String name, final String phone)
                                                    throws LogicException {
        int isRegistered;
        if (UserValidator.isValidUserData(email, phone, name)
                && UserValidator.isValidPassword(password)) {
            UserDaoImpl userDao = new UserDaoImpl();
            try {
                userDao.setConnection();
                if (userDao.findUserIdByEmail(email) == -1) {
                    User user = new User(-1, email, name, phone);
                    user.setPassword(password);
                    userDao.create(user);
                    isRegistered = 1;
                } else {
                    isRegistered = 0;
                }
            } catch (DaoException e) {
                throw new LogicException("Error registration. " + e.getMessage(), e);
            } finally {
                userDao.closeConnection();
            }
        } else {
            isRegistered = -1;
        }
        return isRegistered;
    }

    /**
     * Log in user.
     *
     * @param email    the email
     * @param password the password
     * @return the user
     * @throws LogicException the logic exception
     */
    public User logIn(final String email, final String password) throws LogicException {
        User user = null;
        if (UserValidator.isValidLoginData(email, password)) {
            UserDaoImpl userDao = new UserDaoImpl();
            try {
                userDao.setConnection();
                user = userDao.findUserByEmail(email, password);
            } catch (DaoException e) {
                throw new LogicException("Error log in. " + e.getMessage(), e);
            } finally {
                userDao.closeConnection();
            }
        }
        return user;
    }

    /**
     * Update user data.
     *
     * @param email     the email
     * @param name      the name
     * @param phone     the phone
     * @param id        the id
     * @throws LogicException the logic exception
     */
    public String updateUserData(final String email, final String name, final String phone,
                                 final long id, final boolean emailChanged) throws LogicException {
        String isUpdated = "true";
        if (UserValidator.isValidUserData(email, phone, name)) {
            UserDaoImpl userDao = new UserDaoImpl();
            try {
                userDao.setConnection();
                if (!emailChanged || userDao.findUserIdByEmail(email) == -1) {
                    userDao.update(new User(id, email, name, phone));
                } else {
                    isUpdated = "false";
                }
            } catch (DaoException e) {
                throw new LogicException("Error update info. " + e.getMessage(), e);
            } finally {
                userDao.closeConnection();
            }
        } else {
            isUpdated = "Incorrect";
        }
        return isUpdated;
    }

    /**
     * Finds user information.
     *
     * @param userId the id of user
     * @throws LogicException the logic exception
     */
    public User findUserInformation(final long userId) throws LogicException {
        User user;
        UserDaoImpl userDao = new UserDaoImpl();
        try {
            userDao.setConnection();
            user = userDao.find(userId);
        } catch (DaoException e) {
            throw new LogicException(e.getMessage(), e);
        } finally {
            userDao.closeConnection();
        }
        return user;
    }

    /**
     * Finds service information of the user.
     *
     * @param userId the id of user
     * @throws LogicException the logic exception
     */
    public List<Service> findServiceInformation(final long userId) throws LogicException {
        List<Service> services;
        ServiceDaoImpl serviceDao = new ServiceDaoImpl();
        try {
            serviceDao.setConnection();
            services = serviceDao.findUserServices(userId);
        } catch (DaoException e) {
            throw new LogicException("Error service found." + e.getMessage(), e);
        } finally {
            serviceDao.closeConnection();
        }
        return services;
    }

    /**
     * Recharge the balance of user.
     *
     * @param moneyAmountInString the amount of money on which need recharge
     * @param userId              the id of user
     * @return amount of money in BigDecimal format,
     *          but if data isn't correct then null
     * @throws LogicException the logic exception
     */
    public BigDecimal rechargeUserBalance(final String moneyAmountInString,
                                       final long userId) throws LogicException {
        BigDecimal moneyAmount = null;
        if (UtilValidator.isValidPrice(moneyAmountInString)) {
            moneyAmount = DecimalParser.parseToBigDecimal(moneyAmountInString);
            log.debug("bigDecimal = " + moneyAmount);
            Transaction transaction = null;
            try {
                transaction = new Transaction();
                UserDaoImpl userDao = new UserDaoImpl(transaction.getConnection());

                BigDecimal currentBalance = userDao.findCurrentBalance(userId);
                moneyAmount = userDao.rechargeBalance(currentBalance, moneyAmount, userId);

                transaction.commit();
            } catch (DaoException e) {
                if (transaction != null) {
                    transaction.rollback();
                }
                throw new LogicException("Error of creation." + e.getMessage(), e);
            } finally {
                if (transaction != null) {
                    transaction.closeConnection();
                }
            }
        }
        return moneyAmount;
    }

    /**
     * Finds user overdraft.
     *
     * @param userId the id of the user
     * @return user overdraft
     * @throws LogicException the logic exception
     */
    public BigDecimal findUserOverdraft(final long userId) throws LogicException {
        BigDecimal invoicesSummary;
        BigDecimal paymentsSummary;
        BigDecimal userOverdraft;
        Transaction transaction = null;

        try {
            transaction = new Transaction();
            UserDaoImpl userDao = new UserDaoImpl(transaction.getConnection());

            invoicesSummary = userDao.findInvoicesSummary(userId);
            paymentsSummary = userDao.findPaymentsSummary(userId);
            if(invoicesSummary == null) {
                userOverdraft = BigDecimal.ZERO;
            } else if(paymentsSummary == null) {
                userOverdraft = invoicesSummary;
            } else {
                userOverdraft = invoicesSummary.subtract(paymentsSummary);
            }
            transaction.commit();
        } catch (DaoException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new LogicException("Error of creation." + e.getMessage(), e);
        } finally {
            if (transaction != null) {
                transaction.closeConnection();
            }
        }
        return userOverdraft;
    }

    /**
     * Search of the users which contains pattern string in name or email.
     *
     * @param searchQuery the string with search query
     * @throws LogicException the logic exception
     */
    public List<User> searchOfUsers(final String searchQuery) throws LogicException {
        List<User> users;
        UserDaoImpl userDao = new UserDaoImpl();
        try {
            userDao.setConnection();
            if (!searchQuery.isEmpty()) {
                users = userDao.searchOfUsers(searchQuery);
            } else {
                users = userDao.findAll();
            }
        } catch (DaoException e) {
            throw new LogicException("Error users found." + e.getMessage(), e);
        } finally {
            userDao.closeConnection();
        }
        return users;
    }

}
