package by.training.invoice.logic;

import by.training.invoice.dao.impl.ServiceDaoImpl;
import by.training.invoice.entity.impl.Service;
import by.training.invoice.exception.DaoException;
import by.training.invoice.exception.LogicException;
import by.training.invoice.util.DecimalParser;
import by.training.invoice.validator.UtilValidator;

import java.math.BigDecimal;

/**
 * The type Service logic.
 * Created on 04.01.2018.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class ServiceLogic {
    /**
     * Create service.
     *
     * @param title       the title
     * @param description the description
     * @param price       the price
     * @param sellerId    the seller id
     * @return the boolean
     * @throws LogicException the logic exception
     */
    public boolean create(final String title, final String description,
                          final String price, final long sellerId)
            throws LogicException {
        boolean isCreated = true;
        if (UtilValidator.isValidPrice(price)) {
            BigDecimal servicePrice = DecimalParser.parseToBigDecimal(price);
            ServiceDaoImpl serviceDao = new ServiceDaoImpl();
            try {
                serviceDao.setConnection();
                serviceDao.create(new Service(-1, title, description,
                                                servicePrice, sellerId));
            } catch (DaoException e) {
                throw new LogicException("Error of creation."
                                        + e.getMessage(), e);
            } finally {
                serviceDao.closeConnection();
            }
        } else {
            isCreated = false;
        }
        return isCreated;
    }

    /**
     * Archives service.
     *
     * @param id the id
     * @throws LogicException the logic exception
     */
    public void remove(final long id) throws LogicException {
        ServiceDaoImpl serviceDao = new ServiceDaoImpl();
        try {
            serviceDao.setConnection();
            serviceDao.remove(id);
        } catch (DaoException e) {
            throw new LogicException("Error registration." + e.getMessage(), e);
        } finally {
            serviceDao.closeConnection();
        }
    }
}
