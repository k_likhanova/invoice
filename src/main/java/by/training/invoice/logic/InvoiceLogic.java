package by.training.invoice.logic;

import by.training.invoice.dao.Transaction;
import by.training.invoice.dao.impl.ExpenseDaoImpl;
import by.training.invoice.dao.impl.InvoiceDaoImpl;
import by.training.invoice.dao.impl.PaymentDaoImpl;
import by.training.invoice.dao.impl.UserDaoImpl;
import by.training.invoice.entity.impl.Invoice;
import by.training.invoice.entity.impl.User;
import by.training.invoice.exception.DaoException;
import by.training.invoice.exception.LogicException;
import by.training.invoice.util.DecimalParser;
import by.training.invoice.validator.UtilValidator;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

/**
 * The type Invoice logic.
 * Created on 05.01.2019.
 *
 * @author Kseniya Likhanova
 * @version 1.0
 */
public class InvoiceLogic {

    /**
     * Create invoice.
     *
     * @param title           the title
     * @param description     the description
     * @param paymentDeadline the payment deadline
     * @param sellerId        the seller id
     * @param customerEmail   the customer email
     * @param customerId      the customer id
     * @param serviceIds      the services id
     * @param serviceAmount   the service amount
     * @return the boolean
     * @throws LogicException the logic exception
     */
    public boolean create(final String title, final String description,
                       final Date paymentDeadline, final long sellerId,
                       final String customerEmail, long customerId,
                       final String[] serviceIds, final String[] serviceAmount)
                                                        throws LogicException {
        boolean isCreated = true;
        User customer = new User();
        User seller = new User();
        boolean isApproved = false;
        seller.setId(sellerId);
        Transaction transaction = null;
        try {
            transaction = new Transaction();
            InvoiceDaoImpl invoiceDao = new InvoiceDaoImpl(transaction.getConnection());
            ExpenseDaoImpl expenseDao = new ExpenseDaoImpl(transaction.getConnection());
            UserDaoImpl userDao = new UserDaoImpl(transaction.getConnection());

            if (customerEmail != null) {
                customerId = userDao.findUserIdByEmail(customerEmail);
                isApproved = true;
            }
            if (customerId != -1) {
                customer.setId(customerId);
                Invoice invoice = new Invoice(-1, title, description, paymentDeadline,
                                                seller, customer, isApproved);
                invoice.setId(invoiceDao.create(invoice));
                for (int i = 0; i < serviceIds.length; i++) {
                    expenseDao.create(invoice.getId(), Long.parseLong(serviceIds[i]),
                                                       Integer.parseInt(serviceAmount[i]));
                }
            } else {
                isCreated = false;
            }
            transaction.commit();
        } catch (DaoException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new LogicException("Error of creation." + e.getMessage(), e);
        } finally {
            if (transaction != null) {
                transaction.closeConnection();
            }
        }
        return isCreated;
    }

    /**
     * Finds invoice.
     *
     * @param invoiceId the id of the invoice
     * @return invoice invoice
     * @throws LogicException the logic exception
     */
    public Invoice find(final long invoiceId) throws LogicException {
        Invoice invoice;
        Transaction transaction = null;
        try {
            transaction = new Transaction();
            InvoiceDaoImpl invoiceDao = new InvoiceDaoImpl(transaction.getConnection());
            ExpenseDaoImpl expenseDao = new ExpenseDaoImpl(transaction.getConnection());
            PaymentDaoImpl paymentDao = new PaymentDaoImpl(transaction.getConnection());

            invoice = invoiceDao.find(invoiceId);
            invoice.setExpenses(expenseDao.findAll(invoiceId));
            invoice.setPayments(paymentDao.findAll(invoiceId));
            invoice.setTotalPrice(invoiceDao.findInvoiceTotalPrice(invoiceId));
            invoice.setPaymentsSum(invoiceDao.findSumOfAllInvoicePayments(invoiceId));

            transaction.commit();
        } catch (DaoException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new LogicException("Error of find invoice." + e.getMessage(), e);
        } finally {
            if (transaction != null) {
                transaction.closeConnection();
            }
        }
        return invoice;
    }

    /**
     * Finds invoice.
     *
     * @param userId the id of the user
     * @return list of the invoices
     * @throws LogicException the logic exception
     */
    public List<Invoice> findAll(final long userId) throws LogicException {
        List<Invoice> invoices;
        List<BigDecimal> invoicesPayments;
        Transaction transaction = null;
        try {
            transaction = new Transaction();
            InvoiceDaoImpl invoiceDao = new InvoiceDaoImpl(transaction.getConnection());

            invoices = invoiceDao.findInvoices(userId);
            invoicesPayments = invoiceDao.findInvoicesPaidPartsForUser(userId);

            for (int i = 0; i < invoices.size(); i++) {
                invoices.get(i).setPaymentsSum(invoicesPayments.get(i));
            }
            transaction.commit();
        } catch (DaoException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new LogicException("Error of find invoices." + e.getMessage(), e);
        } finally {
            if (transaction != null) {
                transaction.closeConnection();
            }
        }
        return invoices;
    }

    /**
     * Remove invoice.
     *
     * @param id     the id of the invoice
     * @param userId the seller or the customer id
     * @return the boolean
     * @throws LogicException the logic exception
     */
    public boolean remove(final long id, final long userId) throws LogicException {
        boolean isRemoved = true;
        BigDecimal totalPrice;
        BigDecimal paymentsSum;
        Transaction transaction = null;
        try {
            transaction = new Transaction();
            InvoiceDaoImpl invoiceDao = new InvoiceDaoImpl(transaction.getConnection());
            long[] identifications = invoiceDao.findSellerAndCustomerId(id);
            boolean isSeller = identifications[0] == userId;
            if (!isSeller) {
                totalPrice = invoiceDao.findInvoiceTotalPrice(id);
                paymentsSum = invoiceDao.findSumOfAllInvoicePayments(id);
                if (paymentsSum == null || totalPrice.compareTo(paymentsSum) != 0) {
                    isRemoved = false;
                }
            }
            invoiceDao.remove(id, isSeller);
            transaction.commit();
        } catch (DaoException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new LogicException("Error of remove." + e.getMessage(), e);
        } finally {
            if (transaction != null) {
                transaction.closeConnection();
            }
        }
        return isRemoved;
    }

    /**
     * Pays the invoice.
     * It's impossible to pay, if
     * 1.wrong format of payment,
     * 2.there isn't enough money on the current balance.
     * If the customer wants to pay more than the his overdraft,
     * then payment is equal to a his overdraft of this invoice.
     *
     * @param paymentInString the amount of money in payment
     * @param invoiceId       the id of the invoice
     * @return payment big decimal
     * @throws LogicException the logic exception
     */
    public BigDecimal pay(final String paymentInString, final long invoiceId) throws LogicException {
        BigDecimal result = null;
        if (UtilValidator.isValidPrice(paymentInString)) {
            BigDecimal payment = DecimalParser.parseToBigDecimal(paymentInString);
            BigDecimal currentBalance;
            long sellerId;
            long customerId;
            BigDecimal sumInvoicePayments;
            BigDecimal invoiceOverdraft = new BigDecimal(-1);
            Transaction transaction = null;

            try {
                transaction = new Transaction();
                UserDaoImpl userDao = new UserDaoImpl(transaction.getConnection());
                PaymentDaoImpl paymentDao = new PaymentDaoImpl(transaction.getConnection());
                InvoiceDaoImpl invoiceDao = new InvoiceDaoImpl(transaction.getConnection());

                long[] identifications = invoiceDao.findSellerAndCustomerId(invoiceId);
                sellerId = identifications[0];
                customerId = identifications[1];
                currentBalance = userDao.findCurrentBalance(customerId);
                sumInvoicePayments = invoiceDao.findSumOfAllInvoicePayments(invoiceId);
                if (sumInvoicePayments != null) {
                    invoiceOverdraft = invoiceDao.findInvoiceTotalPrice(invoiceId)
                                        .subtract(sumInvoicePayments);
                    if ((payment.subtract(invoiceOverdraft))
                            .compareTo(BigDecimal.ZERO) > 0) {
                        payment = invoiceOverdraft;
                    }
                }
                if (invoiceOverdraft.compareTo(BigDecimal.ZERO) != 0
                        && currentBalance.compareTo(payment) >= 0) {
                    result = payment;
                    userDao.pay(currentBalance,payment,customerId);
                    paymentDao.create(payment, invoiceId);
                    userDao.rechargeBalance(userDao.findCurrentBalance(sellerId),
                                            payment, sellerId);
                } else if (invoiceOverdraft.compareTo(BigDecimal.ZERO) == 0){
                    result = BigDecimal.ZERO;
                    transaction.rollback();
                } else {
                    result = BigDecimal.ONE.negate();
                    transaction.rollback();
                }
                transaction.commit();
            } catch (DaoException e) {
                if (transaction != null) {
                    transaction.rollback();
                }
                throw new LogicException("Error of creation." + e.getMessage(), e);
            } finally {
                if (transaction != null) {
                    transaction.closeConnection();
                }
            }
        }
        return result;
    }

    /**
     * Search of the invoices which contains pattern string in title.
     *
     * @param searchQuery the string with search query
     * @param userId      the user id
     * @return the list of the invoices
     * @throws LogicException the logic exception
     */
    public List<Invoice> searchOfInvoices(final String searchQuery,
                                          final long userId) throws LogicException {
        List<Invoice> invoices;
        List<BigDecimal> invoicesPayments;
        Transaction transaction = null;
        try {
            transaction = new Transaction();
            InvoiceDaoImpl invoiceDao = new InvoiceDaoImpl(transaction.getConnection());

            invoices = invoiceDao.searchOfInvoices(searchQuery, userId);
            invoicesPayments = invoiceDao.searchOfInvoicesPaidPartsForUser(searchQuery, userId);

            for (int i = 0; i < invoices.size(); i++) {
                invoices.get(i).setPaymentsSum(invoicesPayments.get(i));
            }
            transaction.commit();
        } catch (DaoException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new LogicException("Error of find invoices." + e.getMessage(), e);
        } finally {
            if (transaction != null) {
                transaction.closeConnection();
            }
        }
        return invoices;
    }

    /**
     * Approve invoice.
     *
     * @param invoiceId the id of invoice
     * @throws LogicException the logic exception
     */
    public void approve(final long invoiceId) throws LogicException {
        InvoiceDaoImpl invoiceDao = new InvoiceDaoImpl();
        try {
            invoiceDao.setConnection();
            invoiceDao.approve(invoiceId);
        } catch (DaoException e) {
            throw new LogicException("Error of approved invoices." + e.getMessage(), e);
        } finally {
            invoiceDao.closeConnection();
        }
    }

    /**
     * Find seller and customer id by invoice id.
     *
     * @param invoiceId the id of invoice
     * @return the array, where [0] = sellerId, [1] = customer_id
     * @throws LogicException the logic exception
     */
    public long[] findSellerAndCustomer(final long invoiceId) throws LogicException {
        long[] identifications;
        InvoiceDaoImpl invoiceDao = new InvoiceDaoImpl();
        try {
            invoiceDao.setConnection();
            identifications = invoiceDao.findSellerAndCustomerId(invoiceId);
        } catch (DaoException e) {
            throw new LogicException("Error of find seller and customer of the invoice." + e.getMessage(), e);
        } finally {
            invoiceDao.closeConnection();
        }
        return identifications;
    }
}
