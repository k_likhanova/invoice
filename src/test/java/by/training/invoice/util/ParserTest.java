package by.training.invoice.util;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class ParserTest {

    @DataProvider(name = "testBigDecimals")
    public static Object[][] bigDecimals() {
        return new Object[][]{
                {"5", new BigDecimal(5)},
                {"5,3", new BigDecimal("5.3")},
                {"5.3", new BigDecimal("5.3")},
                {"5.000,3", new BigDecimal("5000.3")},
                {"5.000.000,3", new BigDecimal("5000000.3")},
                {"5.000.000", new BigDecimal("5000000")},
                {"5,000.3", new BigDecimal("5000.3")},
                {"5,000,000.3", new BigDecimal("5000000.3")},
                {"5,000,000", new BigDecimal("5000000")},
                {"5 000 000", new BigDecimal("5000000")},
                {"5    000    000", new BigDecimal("5000000")},
                {"5 000 000.3", new BigDecimal("5000000.3")},
                {"5 000 000,3", new BigDecimal("5000000.3")}
        };
    }

        @Test(dataProvider = "testBigDecimals")
        public void testParseToBigDecimal(String priceValue, BigDecimal price){
            Assert.assertEquals(DecimalParser.parseToBigDecimal(priceValue), price);
        }
}