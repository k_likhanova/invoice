INSERT INTO `invoice`.`user` (
  `email`, `password`, `current_balance`,
  `name`, `phone`
) VALUES (
  'kseniya.likhanova@gmail.com', '827e66a38bd6c1489cf7b3738e3cbe34', /* MD5(Kseniya29) hash */
  '7', 'Kseniya Likhanova', '+375295827593');
INSERT INTO `invoice`.`user` (
  `email`, `password`, `current_balance`,
  `name`, `phone`
) VALUES (
  'arkadiy.palata6@yandex.ru', 'ca6f438564e16c371da3477c79784d00', /* MD5(Pirozhkov6) hash */
  '3', 'Arkadiy Pirozhkov', '+375292233115');
INSERT INTO `invoice`.`user` (
  `email`, `password`, `current_balance`,
  `name`, `phone`
) VALUES (
  'ivanov@mail.ru', '80c2a93058003125bd22467a75b9de17', /* MD5(Ivanov0) hash */
  '155', 'Ivan Ivanov', '+375440428934');

INSERT INTO `invoice`.`service` (
  `title`, `description`, `price`, `seller_id`
) VALUES (
  'Socks', 'Socks with image P6.', '3', '2');
INSERT INTO `invoice`.`service` (
  `title`, `description`, `price`, `seller_id`
) VALUES (
  'Sweater', 'New Year \'s sweater.', '30', '2');
INSERT INTO `invoice`.`service` (
  `title`, `description`, `price`, `seller_id`
) VALUES (
  'Web application',
  'To develop the web application with use of Java, tomcat, jsp, mysql, GOF patterns.',
  '1000', '1');
INSERT INTO `invoice`.`service` (
  `title`, `description`, `price`, `seller_id`
) VALUES (
  'Laboratory work ',
  'To perform laboratory work on programming in the Java language.',
  '15', '1');

INSERT INTO `invoice`.`invoice` (
  `title`, `description`, `payment_deadline`, `seller_id`, `customer_id`, `is_approved`
) VALUES (
  'Socks for kseniya.likhanova','Happy New Year', '2019-02-20', '2', '1', '1');
INSERT INTO `invoice`.`invoice` (
  `title`, `payment_deadline`, `seller_id`, `customer_id`, `is_approved`
) VALUES (
  'Web application for ivanov', '2019-02-28', '1', '3', '1');
INSERT INTO `invoice`.`invoice` (
  `title`, `payment_deadline`, `seller_id`, `customer_id`, `is_approved`
) VALUES (
  'Laboratory work for ivanov', '2019-02-01', '1', '3', '0');

INSERT INTO `invoice`.`expense` (
  `invoice_id`, `service_id`, `amount`
) VALUES ('1', '1', '2');
INSERT INTO `invoice`.`expense` (
  `invoice_id`, `service_id`, `amount`
) VALUES ('1', '2', '1');
INSERT INTO `invoice`.`expense` (
  `invoice_id`, `service_id`, `amount`
) VALUES ('2', '3', '1');
INSERT INTO `invoice`.`expense` (
  `invoice_id`, `service_id`, `amount`
) VALUES ('3', '4', '2');

INSERT INTO `invoice`.`payment` (`payment`, `invoice_id`) VALUES ('3', '1');
INSERT INTO `invoice`.`payment` (`payment`, `invoice_id`) VALUES ('33', '1');
INSERT INTO `invoice`.`payment` (`payment`, `invoice_id`) VALUES ('12', '2');
