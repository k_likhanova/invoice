USE `invoice`;

CREATE TABLE `user` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(224) NOT NULL,
  `password` CHAR(32) NOT NULL,
  `current_balance` DECIMAL(19,4) UNSIGNED NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `phone` CHAR(17) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `service` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `price` DECIMAL(19,4) UNSIGNED NOT NULL,
  `seller_id` BIGINT NOT NULL,
  `is_archival` tinyint NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`),
  INDEX `FK_service_seller` (`seller_id` ASC),
  CONSTRAINT `FK_service_seller`
    FOREIGN KEY (`seller_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `invoice` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `description` VARCHAR(225) NULL,
  `created_date` DATETIME NOT NULL DEFAULT NOW(),
  `payment_deadline` DATE NOT NULL,
  `seller_id` BIGINT NOT NULL,
  `customer_id` BIGINT NOT NULL,
  `is_archival_for_customer` TINYINT NOT NULL DEFAULT 0 ,
  `is_archival_for_seller` TINYINT NOT NULL DEFAULT 0 ,
  `is_approved` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `FK_invoice_seller` (`seller_id` ASC),
  INDEX `FK_invoice_customer` (`customer_id` ASC),
  CONSTRAINT `FK_invoice_seller`
    FOREIGN KEY (`seller_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_invoice_customer`
    FOREIGN KEY (`customer_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `payment` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `payment` DECIMAL(19,4) UNSIGNED NOT NULL,
  `date` DATETIME NOT NULL DEFAULT NOW(),
  `invoice_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_payment_invoice` (`invoice_id` ASC),
  CONSTRAINT `FK_payment_invoice`
    FOREIGN KEY (`invoice_id`)
    REFERENCES `invoice` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `expense` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `invoice_id` BIGINT NOT NULL,
  `service_id` BIGINT NOT NULL,
  `amount` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_expense_invoice` (`invoice_id` ASC),
  INDEX `FK_expense_service` (`service_id` ASC),
  CONSTRAINT `FK_expense_invoice`
    FOREIGN KEY (`invoice_id`)
    REFERENCES `invoice` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_expense_service`
    FOREIGN KEY (`service_id`)
    REFERENCES `service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
