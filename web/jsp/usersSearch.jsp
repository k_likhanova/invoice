<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale" var="loc"/>
<c:url value="/users.html" var="/login.html"/>

<u:html localTitle="true" title="users" className="usersSearch" menu="true" >
    <div class="block">

        <c:url value="/users.html" var="url"/>
        <form class="search" method="get" action="${url}">
            <input type="hidden" name="command" value="users_search"/>
            <input type="text" id="Search" name="searchQuery" value=""
                   placeholder="<fmt:message bundle="${loc}" key="search"/>" class="input"/>
            <input type="submit" value="&#128269;" class="button button--search"/>
        </form>

        <div class="box box--transparent box--wide list">
            <div class="form__header">
                <fmt:message bundle="${loc}" key="users"/>
            </div>
            <div class="divider form__divider"></div>
            <div class="users">
                <c:choose>
                    <c:when test="${not empty users}">
                        <c:forEach items="${users}" var="userProfile">
                            <c:if test="${userProfile.id != user.id}">
                                <c:url value="/profile/${userProfile.id}.html" var="url"/>
                                <a class="link app-header__elem app-header__elem--link user" href="${url}">
                                    ${userProfile.name}
                                    (${userProfile.email})
                                </a>
                            </c:if>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <fmt:message bundle="${loc}" key="message.users_not_found"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</u:html>
