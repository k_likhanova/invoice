<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale" var="loc"/>
<c:url value="/registration.html" var="/login.html"/>

<u:html localTitle="true" title="registration" className="registration" menu="false">
    <div class="error-block">
        <span class="error-block__message">
            ${sessionScope.errorRegistrationMessage}
        </span>
        <c:remove var="errorRegistrationMessage" scope="session"/>
    </div>
    <div class="local-header">
        <div class="back-link">
            <c:url value="/login.html" var="loginUrl"/>
            <a class="button link" href="${loginUrl}"><fmt:message bundle="${loc}" key="login.back"/></a>
        </div>
        <u:localization pageUrl="${url}"/>
    </div>
    <form method="post" action="${url}" class="registration">
        <div class="main-header">Simple Invoice</div>
        <input type="hidden" name="command" value="registration"/>
        <div class="box box--small registration__form">
            <div class="registration__header"><fmt:message bundle="${loc}" key="registration"/></div>
            <div class="divider registration__divider"></div>
            <label class="registration__label">
                <span class="registration__label-text">E-mail</span>
                <div class="form__input-container">
                    <input required type="email" id="Email" name="email" value=""
                           placeholder="<fmt:message bundle="${loc}" key="placeholder.email"/>"
                           pattern="^[\w.-]+@[a-zA-Z\d.-]+\.[a-z]{2,6}$"
                           title="<fmt:message bundle="${loc}" key="validation.email"/>" class="input"/>
                    <span class="form__input-star">*</span>
                </div>
            </label>
            <label class="registration__label">
                <span class="registration__label-text"><fmt:message bundle="${loc}" key="password"/></span>
                <div class="form__input-container">
                    <input required type="password" id="Password" name="password" value=""
                           placeholder="<fmt:message bundle="${loc}" key="placeholder.password"/>"
                           pattern="(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z])\w{6,20}"
                           title="<fmt:message bundle="${loc}" key="validation.password"/>" class="input"/>
                    <span class="form__input-star">*</span>
                </div>
            </label>
            <label class="registration__label">
                <span class="registration__label-text"><fmt:message bundle="${loc}" key="name"/></span>
                <div class="form__input-container">
                    <input required type="text" id="name" name="name" value=""
                           placeholder="<fmt:message bundle="${loc}" key="placeholder.name"/>"
                           pattern="^.{1,100}$" title="<fmt:message bundle="${loc}" key="validation.name"/>" class="input"/>
                    <span class="form__input-star">*</span>
                </div>
            </label>
            <label class="registration__label">
                <span class="registration__label-text"><fmt:message bundle="${loc}" key="phone"/></span>
                <div class="form__input-container">
                    <input required type="text" id="Phone" name="phone" value="" class="input"
                           placeholder="<fmt:message bundle="${loc}" key="placeholder.phone"/>"
                           pattern="^\+375(29|25|33|44)\d{7}$" title="<fmt:message bundle="${loc}" key="validation.phone"/>"/>
                    <span class="form__input-star">*</span>
                </div>
            </label>
            <div class="form__button-container">
                <input type="reset" value="<fmt:message bundle="${loc}" key="reset"/>" class="button registration__button"/>
                <input type="submit" class="button registration__button"
                       value="<fmt:message bundle="${loc}" key="submit"/>"/>
            </div>
        </div>
    </form>
</u:html>
