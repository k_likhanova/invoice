<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale" var="loc"/>
<c:url value="/invoices.html" var="/login.html"/>

<u:html localTitle="true" title="login" className="login" menu="false">

<div class="error-block">
    <span class="error-block__message">
        ${sessionScope.errorLoginMessage}
    </span>
    <c:remove var="errorLoginMessage" scope="session"/>
</div>
<div class="success-block">
    <span>
        ${sessionScope.successfulRegistrationMessage}
    </span>
    <c:remove var="successfulRegistrationMessage" scope="session"/>
</div>
<div class="local-header">
    <u:localization pageUrl="${url}"/>
</div>
<form method="post" action="${url}" class="login">
    <input type="hidden" name="command" value="login"/>
    <div class="main-header">Simple Invoice</div>
    <div class="box box--small login__form">
        <div class="login__header"><fmt:message bundle="${loc}" key="login"/></div>
        <div class="divider login__divider"></div>
        <label class="login__label">
            <span class="login__label-text">E-mail</span>
            <input required type="text" id="Email" name="email" value=""
                   placeholder="<fmt:message bundle="${loc}" key="placeholder.email"/>" class="input"/>
        </label>
        <label class="login__label">
            <span class="login__label-text"><fmt:message bundle="${loc}" key="password"/></span>
            <input required type="password" id="Password" name="password" value=""
                   placeholder="<fmt:message bundle="${loc}" key="placeholder.password"/>" class="input"/>
        </label>
        <div class="login__buttons-container">
            <div class="login__button-container">
                <c:url value="/registration.html" var="url"/>
                <a class="button login__button" href="${url}"><fmt:message bundle="${loc}" key="registration"/></a>
            </div>
            <div class="login__button-container">
                <input type="submit" value="<fmt:message bundle="${loc}" key="login.submit"/>" class="button login__button"/>
            </div>
        </div>
    </div>
</form>
</u:html>
