<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale" var="loc"/>
<c:url value="/invoices.html" var="url"/>

<u:html localTitle="true" title="invoices" className="invoices" menu="true">
    <div class="block">
        <c:url value="/invoices.html" var="url"/>
        <form class="search" method="get" action="${url}">
            <input type="hidden" name="command" value="invoices_search"/>
            <input type="text" id="Search" name="searchQuery" value=""
                   placeholder="<fmt:message bundle="${loc}" key="search"/>" class="input"/>
            <input type="submit" value="&#128269;" class="button button--search"/>
        </form>

        <div class="box box--transparent box--wide list">
            <div class="form__header">
                <fmt:message bundle="${loc}" key="invoices"/>
                <fmt:message bundle="${loc}" key="invoice_color"/>
            </div>
            <div class="divider form__divider"></div>
            <div class="invoices">
                <c:choose>
                    <c:when test="${not empty invoices}">
                        <c:forEach items="${invoices}" var="invoice">
                            <c:url value="/invoice/${invoice.id}.html" var="url"/>
                            <a class="link app-header__elem app-header__elem--link invoice" href="${url}">
                                <c:choose>
                                    <c:when test="${user.id == invoice.seller.id}">
                                        <div class="invoice__title invoice__title--self">
                                            ${invoice.title}
                                            (${invoice.customer.name})
                                            <c:if test="${not invoice.approved}">
                                                <span class="invoice__approvement-message">
                                                    <fmt:message bundle="${loc}" key="message.invoice_approvement"/>
                                                </span>
                                            </c:if>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="invoice__title">
                                            ${invoice.title}
                                            (${invoice.seller.name})
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                <c:when test="${invoice.paymentsSum == invoice.totalPrice}">
                                <div class="invoice__price invoice__price--paid">
                                    </c:when>
                                    <c:otherwise>
                                        <div class="invoice__price">
                                    </c:otherwise>
                                    </c:choose>
                                        <c:choose>
                                            <c:when test="${not empty invoice.paymentsSum}">
                                                <u:decimalLocalisation moneyAmount="${invoice.paymentsSum}"/>/<u:decimalLocalisation moneyAmount="${invoice.totalPrice}"/>
                                            </c:when>
                                            <c:otherwise>
                                                0/<u:decimalLocalisation moneyAmount="${invoice.totalPrice}"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <form class="invoice__delete" method="post" action="${url}">
                                        <input type="hidden" name="command" value="remove_invoice"/>
                                        <input type="hidden" name="invoiceId" value="${invoice.id}"/>
                                        <input class="button button--small" type="submit" value="X"/>
                                    </form>
                            </a>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <fmt:message bundle="${loc}" key="message.invoices_not_found"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</u:html>
