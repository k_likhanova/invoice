<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale" var="loc"/>
<c:url value="/profile/${userProfile.id}.html" var="/login.html"/>

<u:html localTitle="false" title="${userProfile.name}"
        className="userProfile" menu="true" >
    <div class="blocks">
        <div class="block">
            <c:choose>
            <c:when test="${user.id == userProfile.id}">
            <form method="post" action="${url}" class="box box--half form">
                <input type="hidden" name="command" value="change_user_data"/>
                </c:when>
                <c:otherwise>
                <form method="post" action="${url}" class="box box--half form disabled">
                    </c:otherwise>
                    </c:choose>
                    <div class="form__header"><fmt:message bundle="${loc}" key="profile"/></div>
                    <div class="divider form__divider"></div>
                    <label class="form__label">
                        <span class="form__label-text">E-mail</span>
                        <input required type="email" id="Email" name="email" value="${userProfile.email}"
                               placeholder="<fmt:message bundle="${loc}" key="placeholder.email"/>"
                               pattern="^[\w.-]+@[a-zA-Z\d.-]+\.[a-z]{2,6}$"
                               title="<fmt:message bundle="${loc}" key="validation.email"/>" class="input"/>
                    </label>
                    <label class="form__label">
                        <span class="form__label-text"><fmt:message bundle="${loc}" key="name"/></span>
                        <input required type="text" id="name" name="name" value="${userProfile.name}"
                               placeholder="<fmt:message bundle="${loc}" key="placeholder.name"/>"
                               pattern="^.{1,100}$" title="<fmt:message bundle="${loc}" key="validation.name"/>"
                               class="input"/>
                    </label>
                    <label class="form__label">
                        <span class="form__label-text"><fmt:message bundle="${loc}" key="phone"/></span>
                        <input required type="text" id="Phone" name="phone" value="${userProfile.phone}"
                               placeholder="<fmt:message bundle="${loc}" key="placeholder.phone"/>"
                               pattern="^\+375(29|25|33|44)\d{7}$"
                               title="<fmt:message bundle="${loc}" key="validation.phone"/>" class="input"/>
                    </label>
                    <c:if test="${user.id == userProfile.id}">
                        <div class="form__button-container">
                            <input type="reset" class="button form__button"
                                   value="<fmt:message bundle="${loc}" key="reset"/>"/>
                            <input type="submit" class="button registration__button"
                                   value="<fmt:message bundle="${loc}" key="submit"/>"/>
                        </div>
                    </c:if>
                </form>
                <c:if test="${user.id == userProfile.id}">
                    <div class="finance block">
                        <div>
                            <fmt:message bundle="${loc}" key="current_balance"/> =
                            <u:decimalLocalisation moneyAmount="${userProfile.currentBalance}"/>
                        </div>

                        <form method="post" action="${url}" class="finance__add-form">
                            <input type="hidden" name="command" value="recharge_balance"/>
                            <input type="text" id="moneyAmount" name="moneyAmount" value="0"
                                   class="input finance__add-input"
                                   pattern="^[1-9]\d*(((,\d+)*(\.\d+)?)|((\.\d+)*(,\d+)?))$" required/>
                            <input type="submit" value="<fmt:message bundle="${loc}" key="add_money"/>" class="button"/>
                        </form>
                        <div>
                            <fmt:message bundle="${loc}" key="total_overdraft"/> =
                            <u:decimalLocalisation moneyAmount="${overdraft}"/>
                        </div>
                    </div>
                </c:if>
        </div>
        <div class="services block">
            <div class="box box--half box--transparent">
                <div class="form__header"><fmt:message bundle="${loc}" key="services"/></div>
                <div class="divider form__divider"></div>
                <div>
                    <input id="services" type="hidden" name="serviceId" value="${userServices}"/>
                    <c:forEach items="${userServices}" var="service">
                        <div class="service">
                            <div class="service__add-block">
                                <div>
                                    <div class="service__add" onclick="plusService(${service.id})">
                                        +
                                    </div>
                                    <div class="service__add" onclick="minusService(${service.id})">
                                        -
                                    </div>
                                </div>
                            </div>
                            <div class="service__body">
                                <div class="service__header">
                                    <div class="service__title">
                                        <span class="service__amount" id="service-amount_${service.id}"></span>
                                        ${service.title}
                                    </div>
                                    <div class="service__price">
                                        <u:decimalLocalisation moneyAmount="${service.price}"/>
                                    </div>
                                    <c:if test="${user.id == userProfile.id}">
                                        <form method="post" action="${url}" class="service__delete" >
                                            <input type="hidden" name="command" value="remove_service"/>
                                            <input type="hidden" name="serviceId" value="${service.id}"/>
                                            <input class="button button--small" type="submit" value="X"/>
                                        </form>
                                    </c:if>
                                </div>
                                <div class="service__description">
                                        ${service.description}
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <c:if test="${user.id == userProfile.id}">
                    <button class="button" onclick="openModal('service')">
                        <fmt:message bundle="${loc}" key="add_service"/>
                    </button>
                </c:if>
                <c:if test="${not empty userServices}">
                    <button class="button" onclick="createInvoiceClickHandler()">
                        <fmt:message bundle="${loc}" key="create_invoice"/>
                    </button>
                </c:if>
                <c:if test="${empty userServices}">
                    <fmt:message bundle="${loc}" key="message.no_services"/>
                </c:if>
            </div>
        </div>
    </div>
</u:html>