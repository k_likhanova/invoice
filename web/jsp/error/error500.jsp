<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale" var="loc"/>
<html>
<head>
    <title>Error</title>
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
</head>
<body>
<div class="error-page">
    <span class="error-page__header">Error</span>
    <div class="error-page__message">
        Request from ${pageContext.errorData.requestURI} is failed <br/>
        Servlet name or type: ${pageContext.errorData.servletName} <br/>
        Status code: ${pageContext.errorData.statusCode} <br/>
        Exception: ${pageContext.exception.stackTraceElement.toString}. <br/>
    </div>
    <c:choose>
    <c:when test="${not empty sessionScope.user}">
        <div class="error-page__button-container">
            <c:url value="/profile/${user.id}.html" var="profileUrl"/>
            <a href="${profileUrl}" class="link button"><fmt:message bundle="${loc}" key="error.back"/></a>
        </div>
    </c:when>
    <c:otherwise>
        <div class="error-page__button-container">
            <c:url value="/login.html" var="loginUrl"/>
            <a href="${loginUrl}" class="link button"><fmt:message bundle="${loc}" key="login.back"/></a>
        </div>
    </c:otherwise>
    </c:choose>
</div>
</body>
</html>
