<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale" var="loc"/>
<c:url value="/invoice/${invoice.id}.html" var="url"/>

<u:html localTitle="false" title="${invoice.title}"
        className="invoice" menu="true">
    <div class="block">
        <div class="box box--transparent">
            <div class="form__header invoices-header">
                <fmt:message bundle="${loc}" key="invoice"/> ${invoice.title}
            </div>
            <div class="divider form__divider"></div>
            <div class="invoice">
                <c:if test="${not empty invoice.description}">
                    <div class="invoice__elem">
                        <fmt:message bundle="${loc}" key="invoice.description"/>: ${invoice.description}
                    </div>
                </c:if>
                <div class="invoice__elem">
                    <fmt:message bundle="${loc}" key="invoice.created_date"/>:
                    <fmt:formatDate value="${invoice.createdDate}"/>
                </div>
                <div class="invoice__elem">
                    <fmt:message bundle="${loc}" key="invoice.payment_deadline"/>:
                    <fmt:formatDate value="${invoice.paymentDeadline}"/>
                </div>
                <div class="invoice__elem">
                    <fmt:message bundle="${loc}" key="seller"/>:
                    <c:url value="/profile/${invoice.seller.id}.html" var="sellerUrl"/>
                    <a class="link" href="${sellerUrl}">
                        ${invoice.seller.name}
                    </a>
                </div>
                <div class="invoice__elem">
                    <fmt:message bundle="${loc}" key="customer"/>:
                    <c:url value="/profile/${invoice.customer.id}.html" var="customerUrl"/>
                    <a class="link" href="${customerUrl}">
                        ${invoice.customer.name}
                    </a>
                </div>
                <div class="divider form__divider"></div>
                <div class="list">
                    <div class="list__header">
                        <fmt:message bundle="${loc}" key="ordered_services"/>
                    </div>
                    <div>
                        <c:forEach items="${invoice.expenses}" var="expense">
                            <div class="list-elem">
                                <div class="list-elem__header">
                                    <div class="list-elem__left">
                                        ${expense.service.title} x ${expense.serviceAmount}
                                    </div>
                                    <div class="list-elem__right">
                                        <u:decimalLocalisation moneyAmount="${expense.totalPrice}"/>
                                    </div>
                                </div>
                                <div class="list-elem__description">
                                    ${expense.service.description}
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <div class="list-summary">
                    <fmt:message bundle="${loc}" key="summary"/>:
                    <u:decimalLocalisation moneyAmount="${invoice.totalPrice}"/>
                </div>
                <c:if test="${not empty invoice.payments}">
                    <div class="divider form__divider"></div>
                    <div class="list">
                        <div class="list__header">
                            <fmt:message bundle="${loc}" key="payments"/>
                        </div>
                        <div>
                            <c:forEach items="${invoice.payments}" var="payment">
                                <div class="list-elem">
                                    <div class="list-elem__header">
                                        <div class="list-elem__left invoice-date">
                                            <fmt:formatDate type = "date" value="${payment.date}" dateStyle = "short"/>
                                            <fmt:formatDate type = "time" value="${payment.time}" timeStyle = "medium"/>
                                        </div>
                                        <div class="list-elem__right">
                                            <u:decimalLocalisation moneyAmount="${payment.payment}"/>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="list-summary">
                        <fmt:message bundle="${loc}" key="summary"/>:
                        <u:decimalLocalisation moneyAmount="${invoice.paymentsSum}"/>
                    </div>
                </c:if>
                <c:if test="${user.id == invoice.customer.id && invoice.paymentsSum != invoice.totalPrice}">
                    <div class="divider form__divider"></div>
                    <form method="post" action="${url}" class="finance__add-form">
                        <input type="hidden" name="command" value="pay"/>
                        <input type="hidden" name="customerId" value="${invoice.customer.id}"/>
                        <div class="form__input-container">
                            <input type="text" id="payment" name="payment" value="0"
                                   class="input finance__add-input"
                                   pattern="^[1-9]\d*((,\d+)*(\.\d+)?)|((\.\d+)*(,\d+)?)$" required/>
                            <input type="submit" value="<fmt:message bundle="${loc}" key="pay"/>" class="button"/>
                        </div>
                    </form>
                </c:if>
                <c:if test="${user.id == invoice.seller.id && not invoice.approved}">
                    <div class="divider form__divider"></div>
                    <form method="post" action="${url}" class="form__button-container--right">
                        <input type="hidden" name="command" value="approve_invoice"/>
                        <input type="hidden" name="sellerId" value="${invoice.seller.id}"/>
                        <input type="submit" value="<fmt:message bundle="${loc}" key="invoice.approve"/>"
                               class="button"/>
                    </form>
                </c:if>
            </div>
        </div>
    </div>
</u:html>
