<%@tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="pageUrl" required="true" rtexprvalue="true" type="java.lang.String"%>

<form class="link app-header__elem" method="post" action="${pageUrl}">
    <input type="hidden" name="command" value="locale"/>
    <input type="hidden" name="locale" value="en"/>
    <input class="link__input app-header__elem app-header__elem--link" type="submit" value="EN"/>
</form>
<form class="link app-header__elem" method="post" action="${pageUrl}">
    <input type="hidden" name="command" value="locale"/>
    <input type="hidden" name="locale" value="ru"/>
    <input class="link__input app-header__elem app-header__elem--link" type="submit" value="RU"/>
</form>
<form class="link app-header__elem" method="post" action="${pageUrl}">
    <input type="hidden" name="command" value="locale"/>
    <input type="hidden" name="locale" value="be"/>
    <input class="link__input app-header__elem app-header__elem--link" type="submit" value="BE"/>
</form>