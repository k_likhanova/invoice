<%@tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/custom.tld" prefix="t" %>

<%@attribute name="localTitle" required="true" rtexprvalue="true" type="java.lang.Boolean"%>
<%@attribute name="title" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@attribute name="className" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@attribute name="menu" required="true" rtexprvalue="true" type="java.lang.Boolean"%>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale" var="loc"/>

<!DOCTYPE html>
<html>
<head>
    <c:choose>
        <c:when test="${localTitle}">
            <title><fmt:message bundle="${loc}" key="${title}"/></title>
        </c:when>
        <c:otherwise>
            <title>${title}</title>
        </c:otherwise>
    </c:choose>

    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/${className}.css" rel="stylesheet">
</head>
<body>

<c:if test="${menu}">
    <div class="error-block">
        <span class="error-block__message">${sessionScope.errorMessage}</span>
        <c:remove var="errorMessage" scope="session"/>
    </div>
    <div class="page">
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
</c:if>
        <jsp:doBody/>
<c:if test="${menu}">
    </div>
</c:if>
<footer class="footer"><t:Creator/></footer>
</body>
</html>

<script type="text/javascript">
    <%@include file="/js/main.js" %>
</script>