<%@tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="moneyAmount" required="true" rtexprvalue="true" type="java.math.BigDecimal"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:formatNumber type="currency"
                  maxFractionDigits="4" minFractionDigits="1"
                  currencySymbol=""  value="${moneyAmount}"/>