document.getElementsByTagName('body')[0].addEventListener('click', () => {
    let errorElement = document.querySelector('.error-block');
    errorElement.removeChild(errorElement.children[0]);
    let successElement = document.querySelector('.success-block');
    successElement.removeChild(successElement.children[0]);
});

function openModal (name) {
    document.querySelector('.page').innerHTML += eval(name + 'Modal');
}
function closeModal () {
    document.querySelector('.page').removeChild(document.querySelector('.modal'));
}

function fillHiddenInput() {
    document.getElementById("serviceIds").value = ids.toString();
    document.getElementById("serviceAmounts").value = amounts.toString();
}

const serviceModal = `
                <div class="modal">
                    <form method="post" action="${url}" class="box form">
                        <input type="hidden" name="command" value="create_service"/>
                        <div class="form__header">
                            <div class="form__title">
                                <fmt:message bundle="${loc}" key="create_service"/>
                            </div>
                            <div onclick="closeModal()" class="form__close">
                                X
                            </div>
                        </div>
                        <div class="divider form__divider"></div>
                        <label class="form__label">
                            <span class="form__label-text"><fmt:message bundle="${loc}" key="service.title"/></span>
                            <div class="form__input-container">
                                <input required type="text" id="serviceTitle" name="title" value="" class="input"
                                    placeholder="<fmt:message bundle="${loc}" key="service.placeholder.title"/>"/>
                                <span class="form__input-star">*</span>
                            </div>
                        </label>
                        <label class="form__label">
                            <span class="form__label-text"><fmt:message bundle="${loc}" key="service.description"/></span>
                            <div class="form__input-container">
                                <input type="text" id="serviceDescription" name="description" value="" class="input"
                                    placeholder="<fmt:message bundle="${loc}" key="service.placeholder.description"/>"/>
                                <span class="form__input-star">&nbsp;&nbsp;</span>
                            </div>  
                        </label>
                        <label class="form__label">
                            <span class="form__label-text"><fmt:message bundle="${loc}" key="service.price"/></span>
                            <div class="form__input-container">
                                <input required type="text" id="price" name="price" value="0" class="input"
                                    pattern="^[1-9]\\d*(((,\\d+)*(\\.\\d+)?)|((\\.\\d+)*(,\\d+)?))$"
                                    placeholder="<fmt:message bundle="${loc}" key="service.placeholder.price"/>" />
                                <span class="form__input-star">*</span>
                            </div>
                        </label>
                        <div class="form__button-container">
                            <input type="reset" class="button form__button"
                                    value="<fmt:message bundle="${loc}" key="reset"/>" />
                            <input type="submit" class="button form__button"
                                    value="<fmt:message bundle="${loc}" key="create_service"/>" />
                        </div>
                    </form>
                </div>
            `;
const invoiceModal = `
                <div class="modal">
                    <form method="post" action="${url}" class="box form">
                        <input type="hidden" name="command" value="create_invoice"/>
                        <input type="hidden" id="serviceIds" name="services" value=""/>
                        <input type="hidden" id="serviceAmounts" name="serviceAmounts" value=""/>
                        
                        <div class="form__header">
                            <div class="form__title">
                                <fmt:message bundle="${loc}" key="create_invoice"/>
                            </div>
                            <div onclick="closeModal()" class="form__close">
                                X
                            </div>
                        </div>
                        <div class="divider form__divider"></div>
                        <label class="form__label">
                            <span class="form__label-text"><fmt:message bundle="${loc}" key="invoice.title"/></span>
                            <div class="form__input-container">
                                <input required type="text" id="invoiceTitle" name="title" value="" class="input"
                                    placeholder="<fmt:message bundle="${loc}" key="invoice.placeholder.title"/>"/>
                                <span class="form__input-star">*</span>
                            </div>
                        </label>
                        <label class="form__label">
                            <span class="form__label-text">
                                <fmt:message bundle="${loc}" key="invoice.description"/>
                            </span>
                            <div class="form__input-container">
                                <input type="text" id="invoiceDescription" name="description" value="" class="input" 
                                    placeholder="<fmt:message bundle="${loc}" key="invoice.placeholder.description"/>"/>
                                <span class="form__input-star">&nbsp;&nbsp;</span>
                            </div>
                        </label>
                        <label class="form__label">
                            <span class="form__label-text">
                                <fmt:message bundle="${loc}" key="invoice.payment_deadline"/>
                            </span>
                            <div class="form__input-container">
                                <input required type="date" id="invoicePaymentDeadline" name="paymentDeadline" value=""
                                    placeholder="15.01.2018" class="input"/>
                                <span class="form__input-star">*</span>
                            </div>
                        </label>
                        <c:if test="${user.id == userProfile.id}">
                            <label class="form__label">
                                <span class="form__label-text">
                                    <fmt:message bundle="${loc}" key="invoice.customer_email"/>
                                </span>
                                <div class="form__input-container">
                                    <input required type="email" id="customerEmail" name="customerEmail" value=""
                                        placeholder="customer@google.com" class="input"/>
                                    <span class="form__input-star">*</span>
                                </div>
                            </label>
                        </c:if>
                        <div class="form__button-container">
                            <input type="reset" class="button form__button"
                                    value="<fmt:message bundle="${loc}" key="reset"/>" />
                            <input type="submit" class="button form__button" onclick="fillHiddenInput()" 
                                    value="<fmt:message bundle="${loc}" key="create_invoice"/>"/>
                        </div>
                    </form>
                </div>
            `;
// Take this vars for back
let ids = [];
let amounts = [];

// Index of this array is id of appropriate service
let amountsWithIdsIndexes = [];
function minusService (id) {
    if (amountsWithIdsIndexes[id] > 0) {
        amountsWithIdsIndexes[id] = amountsWithIdsIndexes[id] - 1;
        rerenderAmount(id);
    }
}
function plusService (id) {
    if (typeof amountsWithIdsIndexes[id] === 'number') {
        amountsWithIdsIndexes[id]++;
    } else {
        amountsWithIdsIndexes[id] = 1;
    }
    rerenderAmount(id);

}

function createInvoiceClickHandler() {
    let sum = 0;
    amountsWithIdsIndexes.forEach(number => {
        if (number && typeof number === 'number') {
            sum++;
        }
    });
    if (!amountsWithIdsIndexes.length || !sum) {
        alert('Choose services please');
        return
    }
    getServices(amountsWithIdsIndexes);
    openModal('invoice');
}

function getServices (array) {
    array.forEach((amount, id) => {
        ids.push(id);
        amounts.push(amount);
    });
}
function rerenderAmount(id) {
    document.getElementById('service-amount_' + id).innerHTML = amountsWithIdsIndexes[id] + ' x';
}